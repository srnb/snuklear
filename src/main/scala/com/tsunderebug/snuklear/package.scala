package com.tsunderebug

import scala.scalanative.native._

package object snuklear {

  @extern
  @link("libnuklear")
  object nuklear {
    type nk_char = CChar
    type nk_uchar = CUnsignedChar
    type nk_byte = CUnsignedChar
    type nk_short = CShort
    type nk_ushort = CUnsignedShort
    type nk_int = CInt
    type nk_uint = CUnsignedInt
    type nk_size = CUnsignedLong
    type nk_ptr = CUnsignedLong
    type nk_hash = nk_uint
    type nk_flags = nk_uint
    type nk_rune = nk_uint
    type nk_buffer = extern
    type nk_allocator = extern
    type nk_command_buffer = extern
    type nk_draw_command = extern
    type nk_convert_config = extern
    type nk_style_item = extern
    type nk_text_edit = extern
    type nk_draw_list = extern
    type nk_user_font = extern
    type nk_panel = extern
    type nk_context = extern
    type nk_draw_vertex_layout_element = extern
    type nk_style_button = extern
    type nk_style_toggle = extern
    type nk_style_selectable = extern
    type nk_style_slide = extern
    type nk_style_progress = extern
    type nk_style_scrollbar = extern
    type nk_style_edit = extern
    type nk_style_property = extern
    type nk_style_chart = extern
    type nk_style_combo = extern
    type nk_style_tab = extern
    type nk_style_window_header = extern
    type nk_style_window = extern

    type nk_color = CStruct4[
      nk_byte,
      nk_byte,
      nk_byte,
      nk_byte
    ]
    type nk_colorf = CStruct4[
      CFloat,
      CFloat,
      CFloat,
      CFloat
    ]
    type nk_vec2 = CStruct2[
      CFloat,
      CFloat
    ]
    type nk_vec2i = CStruct2[
      CShort,
      CShort
    ]
    type nk_rect = CStruct4[
      CFloat,
      CFloat,
      CFloat,
      CFloat
    ]
    type nk_recti = CStruct4[
      CShort,
      CShort,
      CShort,
      CShort
    ]
    type nk_glyph = CString
    type nk_handle = CInt
    type nk_image = CStruct4[
      nk_handle,
      CUnsignedShort,
      CUnsignedShort,
      Ptr[CUnsignedShort]
    ]
    type nk_cursor = CStruct3[
      nk_image,
      nk_vec2,
      nk_vec2
    ]
    type nk_scroll = CStruct2[
      nk_uint,
      nk_uint
    ]

    type nk_heading = CInt
    type nk_button_behavior = CInt
    type nk_collapse_states = CBool
    type nk_show_states = CBool
    type nk_chart_type = CInt
    type nk_color_format = CInt
    type nk_popup_type = CInt
    type nk_layout_format = CInt
    type nk_tree_type = CInt

    type nk_plugin_alloc = CFunctionPtr3[nk_handle, Ptr[Byte], nk_size, Ptr[Byte]]
    type nk_plugin_free = CFunctionPtr2[nk_handle, Ptr[Byte], Unit]
    type nk_plugin_filter = CFunctionPtr2[Ptr[nk_text_edit], nk_rune, CInt]
    type nk_plugin_paste = CFunctionPtr2[nk_handle, Ptr[nk_text_edit], Unit]
    type nk_plugin_copy = CFunctionPtr3[nk_handle, CString, CInt, Unit]
    type nk_allocator = CStruct3[
      nk_handle,
      nk_plugin_alloc,
      nk_plugin_free
    ]
    type nk_symbol_type = CInt

    def nk_init_fixed(param0: Ptr[nk_context], memory: Ptr[Byte], size: nk_size, param3: Ptr[nk_user_font]): CInt = extern

    def nk_init(param0: Ptr[nk_context], param1: Ptr[nk_allocator], param2: Ptr[nk_user_font]): CInt = extern

    def nk_init_custom(param0: Ptr[nk_context], cmds: Ptr[nk_buffer], pool: Ptr[nk_buffer], param3: Ptr[nk_user_font]): CInt = extern

    def nk_clear(param0: Ptr[nk_context]): Unit = extern

    def nk_free(param0: Ptr[nk_context]): Unit = extern

    type nk_keys = CInt

    type nk_buttons = CInt

    def nk_input_begin(param0: Ptr[nk_context]): Unit = extern

    def nk_input_motion(param0: Ptr[nk_context], x: CInt, y: CInt): Unit = extern

    def nk_input_key(param0: Ptr[nk_context], param1: nk_keys, down: CInt): Unit = extern

    def nk_input_button(param0: Ptr[nk_context], param1: nk_buttons, x: CInt, y: CInt, down: CInt): Unit = extern

    def nk_input_scroll(param0: Ptr[nk_context], `val`: nk_vec2): Unit = extern

    def nk_input_char(param0: Ptr[nk_context], param1: CChar): Unit = extern

    def nk_input_glyph(param0: Ptr[nk_context], param1: nk_glyph): Unit = extern

    def nk_input_unicode(param0: Ptr[nk_context], param1: nk_rune): Unit = extern

    def nk_input_end(param0: Ptr[nk_context]): Unit = extern

    type nk_draw_null_texture = CStruct2[
      nk_handle,
      nk_vec2
    ]

    type nk_anti_aliasing = CInt

    type nk_convert_config = CStruct10[
      CFloat,
      nk_anti_aliasing,
      nk_anti_aliasing,
      CUnsignedInt,
      CUnsignedInt,
      CUnsignedInt,
      nk_draw_null_texture,
      Ptr[nk_draw_vertex_layout_element],
      nk_size,
      nk_size
    ]

    def nk__begin(param0: Ptr[nk_context]): Ptr[nk_command] = extern

    def nk__next(param0: Ptr[nk_context], param1: Ptr[nk_command]): Ptr[nk_command] = extern

    def nk_begin(ctx: Ptr[nk_context], title: CString, bounds: nk_rect, flags: nk_flags): CInt = extern

    def nk_begin_titled(ctx: Ptr[nk_context], name: CString, title: CString, bounds: nk_rect, flags: nk_flags): CInt = extern

    def nk_end(ctx: Ptr[nk_context]): Unit = extern

    def nk_window_find(ctx: Ptr[nk_context], name: CString): Ptr[nk_window] = extern

    def nk_window_get_bounds(ctx: Ptr[nk_context]): nk_rect = extern

    def nk_window_get_position(ctx: Ptr[nk_context]): nk_vec2 = extern

    def nk_window_get_size(param0: Ptr[nk_context]): nk_vec2 = extern

    def nk_window_get_width(param0: Ptr[nk_context]): CFloat = extern

    def nk_window_get_height(param0: Ptr[nk_context]): CFloat = extern

    def nk_window_get_panel(param0: Ptr[nk_context]): Ptr[nk_panel] = extern

    def nk_window_get_content_region(param0: Ptr[nk_context]): nk_rect = extern

    def nk_window_get_content_region_min(param0: Ptr[nk_context]): nk_vec2 = extern

    def nk_window_get_content_region_max(param0: Ptr[nk_context]): nk_vec2 = extern

    def nk_window_get_content_region_size(param0: Ptr[nk_context]): nk_vec2 = extern

    def nk_window_get_canvas(param0: Ptr[nk_context]): Ptr[nk_command_buffer] = extern

    def nk_window_has_focus(param0: Ptr[nk_context]): CInt = extern

    def nk_window_is_hovered(param0: Ptr[nk_context]): CInt = extern

    def nk_window_is_collapsed(ctx: Ptr[nk_context], name: CString): CInt = extern

    def nk_window_is_closed(param0: Ptr[nk_context], param1: CString): CInt = extern

    def nk_window_is_hidden(param0: Ptr[nk_context], param1: CString): CInt = extern

    def nk_window_is_active(param0: Ptr[nk_context], param1: CString): CInt = extern

    def nk_window_is_any_hovered(param0: Ptr[nk_context]): CInt = extern

    def nk_item_is_any_active(param0: Ptr[nk_context]): CInt = extern

    def nk_window_set_bounds(param0: Ptr[nk_context], name: CString, bounds: nk_rect): Unit = extern

    def nk_window_set_position(param0: Ptr[nk_context], name: CString, pos: nk_vec2): Unit = extern

    def nk_window_set_size(param0: Ptr[nk_context], name: CString, param2: nk_vec2): Unit = extern

    def nk_window_set_focus(param0: Ptr[nk_context], name: CString): Unit = extern

    def nk_window_close(ctx: Ptr[nk_context], name: CString): Unit = extern

    def nk_window_collapse(param0: Ptr[nk_context], name: CString, state: nk_collapse_states): Unit = extern

    def nk_window_collapse_if(param0: Ptr[nk_context], name: CString, param2: nk_collapse_states, cond: CInt): Unit = extern

    def nk_window_show(param0: Ptr[nk_context], name: CString, param2: nk_show_states): Unit = extern

    def nk_window_show_if(param0: Ptr[nk_context], name: CString, param2: nk_show_states, cond: CInt): Unit = extern

    def nk_layout_set_min_row_height(param0: Ptr[nk_context], height: CFloat): Unit = extern

    def nk_layout_reset_min_row_height(param0: Ptr[nk_context]): Unit = extern

    def nk_layout_widget_bounds(param0: Ptr[nk_context]): nk_rect = extern

    def nk_layout_ratio_from_pixel(param0: Ptr[nk_context], pixel_width: CFloat): CFloat = extern

    def nk_layout_row_dynamic(ctx: Ptr[nk_context], height: CFloat, cols: CInt): Unit = extern

    def nk_layout_row_static(ctx: Ptr[nk_context], height: CFloat, item_width: CInt, cols: CInt): Unit = extern

    def nk_layout_row_begin(ctx: Ptr[nk_context], fmt: nk_layout_format, row_height: CFloat, cols: CInt): Unit = extern

    def nk_layout_row_push(param0: Ptr[nk_context], value: CFloat): Unit = extern

    def nk_layout_row_end(param0: Ptr[nk_context]): Unit = extern

    def nk_layout_row(param0: Ptr[nk_context], param1: nk_layout_format, height: CFloat, cols: CInt, ratio: Ptr[CFloat]): Unit = extern

    def nk_layout_row_template_begin(param0: Ptr[nk_context], row_height: CFloat): Unit = extern

    def nk_layout_row_template_push_dynamic(param0: Ptr[nk_context]): Unit = extern

    def nk_layout_row_template_push_variable(param0: Ptr[nk_context], min_width: CFloat): Unit = extern

    def nk_layout_row_template_push_static(param0: Ptr[nk_context], width: CFloat): Unit = extern

    def nk_layout_row_template_end(param0: Ptr[nk_context]): Unit = extern

    def nk_layout_space_begin(param0: Ptr[nk_context], param1: nk_layout_format, height: CFloat, widget_count: CInt): Unit = extern

    def nk_layout_space_push(param0: Ptr[nk_context], bounds: nk_rect): Unit = extern

    def nk_layout_space_end(param0: Ptr[nk_context]): Unit = extern

    def nk_layout_space_bounds(param0: Ptr[nk_context]): nk_rect = extern

    def nk_layout_space_to_screen(param0: Ptr[nk_context], param1: nk_vec2): nk_vec2 = extern

    def nk_layout_space_to_local(param0: Ptr[nk_context], param1: nk_vec2): nk_vec2 = extern

    def nk_layout_space_rect_to_screen(param0: Ptr[nk_context], param1: nk_rect): nk_rect = extern

    def nk_layout_space_rect_to_local(param0: Ptr[nk_context], param1: nk_rect): nk_rect = extern

    def nk_group_begin(param0: Ptr[nk_context], title: CString, param2: nk_flags): CInt = extern

    def nk_group_begin_titled(param0: Ptr[nk_context], name: CString, title: CString, param3: nk_flags): CInt = extern

    def nk_group_end(param0: Ptr[nk_context]): Unit = extern

    def nk_group_scrolled_offset_begin(param0: Ptr[nk_context], x_offset: Ptr[nk_uint], y_offset: Ptr[nk_uint], title: CString, flags: nk_flags): CInt = extern

    def nk_group_scrolled_begin(param0: Ptr[nk_context], off: Ptr[nk_scroll], title: CString, param3: nk_flags): CInt = extern

    def nk_group_scrolled_end(param0: Ptr[nk_context]): Unit = extern

    def nk_tree_push_hashed(param0: Ptr[nk_context], param1: nk_tree_type, title: CString, initial_state: nk_collapse_states, hash: CString, len: CInt, seed: CInt): CInt = extern

    def nk_tree_image_push_hashed(param0: Ptr[nk_context], param1: nk_tree_type, param2: nk_image, title: CString, initial_state: nk_collapse_states, hash: CString, len: CInt, seed: CInt): CInt = extern

    def nk_tree_pop(param0: Ptr[nk_context]): Unit = extern

    def nk_tree_state_push(param0: Ptr[nk_context], param1: nk_tree_type, title: CString, state: Ptr[nk_collapse_states]): CInt = extern

    def nk_tree_state_image_push(param0: Ptr[nk_context], param1: nk_tree_type, param2: nk_image, title: CString, state: Ptr[nk_collapse_states]): CInt = extern

    def nk_tree_state_pop(param0: Ptr[nk_context]): Unit = extern

    def nk_tree_element_push_hashed(param0: Ptr[nk_context], param1: nk_tree_type, title: CString, initial_state: nk_collapse_states, selected: Ptr[CInt], hash: CString, len: CInt, seed: CInt): CInt = extern

    def nk_tree_element_image_push_hashed(param0: Ptr[nk_context], param1: nk_tree_type, param2: nk_image, title: CString, initial_state: nk_collapse_states, selected: Ptr[CInt], hash: CString, len: CInt, seed: CInt): CInt = extern

    def nk_tree_element_pop(param0: Ptr[nk_context]): Unit = extern

    type nk_list_view = CStruct7[
      CInt,
      CInt,
      CInt,
      CInt,
      Ptr[nk_context],
      Ptr[nk_uint],
      nk_uint
    ]

    def nk_list_view_begin(param0: Ptr[nk_context], out: Ptr[nk_list_view], id: CString, param3: nk_flags, row_height: CInt, row_count: CInt): CInt = extern

    def nk_list_view_end(param0: Ptr[nk_list_view]): Unit = extern

    type nk_widget_layout_states = CInt

    def nk_widget(param0: Ptr[nk_rect], param1: Ptr[nk_context]): nk_widget_layout_states = extern

    def nk_widget_fitting(param0: Ptr[nk_rect], param1: Ptr[nk_context], param2: nk_vec2): nk_widget_layout_states = extern

    def nk_widget_bounds(param0: Ptr[nk_context]): nk_rect = extern

    def nk_widget_position(param0: Ptr[nk_context]): nk_vec2 = extern

    def nk_widget_size(param0: Ptr[nk_context]): nk_vec2 = extern

    def nk_widget_width(param0: Ptr[nk_context]): CFloat = extern

    def nk_widget_height(param0: Ptr[nk_context]): CFloat = extern

    def nk_widget_is_hovered(param0: Ptr[nk_context]): CInt = extern

    def nk_widget_is_mouse_clicked(param0: Ptr[nk_context], param1: nk_buttons): CInt = extern

    def nk_widget_has_mouse_click_down(param0: Ptr[nk_context], param1: nk_buttons, down: CInt): CInt = extern

    def nk_spacing(param0: Ptr[nk_context], cols: CInt): Unit = extern


    def nk_text(param0: Ptr[nk_context], param1: CString, param2: CInt, param3: nk_flags): Unit = extern

    def nk_text_colored(param0: Ptr[nk_context], param1: CString, param2: CInt, param3: nk_flags, param4: nk_color): Unit = extern

    def nk_text_wrap(param0: Ptr[nk_context], param1: CString, param2: CInt): Unit = extern

    def nk_text_wrap_colored(param0: Ptr[nk_context], param1: CString, param2: CInt, param3: nk_color): Unit = extern

    def nk_label(param0: Ptr[nk_context], param1: CString, align: nk_flags): Unit = extern

    def nk_label_colored(param0: Ptr[nk_context], param1: CString, align: nk_flags, param3: nk_color): Unit = extern

    def nk_label_wrap(param0: Ptr[nk_context], param1: CString): Unit = extern

    def nk_label_colored_wrap(param0: Ptr[nk_context], param1: CString, param2: nk_color): Unit = extern

    def nk_image(param0: Ptr[nk_context], param1: nk_image): Unit = extern

    def nk_image_color(param0: Ptr[nk_context], param1: nk_image, param2: nk_color): Unit = extern

    def nk_button_text(param0: Ptr[nk_context], title: CString, len: CInt): CInt = extern

    def nk_button_label(param0: Ptr[nk_context], title: CString): CInt = extern

    def nk_button_color(param0: Ptr[nk_context], param1: nk_color): CInt = extern

    def nk_button_symbol(param0: Ptr[nk_context], param1: nk_symbol_type): CInt = extern

    def nk_button_image(param0: Ptr[nk_context], img: nk_image): CInt = extern

    def nk_button_symbol_label(param0: Ptr[nk_context], param1: nk_symbol_type, param2: CString, text_alignment: nk_flags): CInt = extern

    def nk_button_symbol_text(param0: Ptr[nk_context], param1: nk_symbol_type, param2: CString, param3: CInt, alignment: nk_flags): CInt = extern

    def nk_button_image_label(param0: Ptr[nk_context], img: nk_image, param2: CString, text_alignment: nk_flags): CInt = extern

    def nk_button_image_text(param0: Ptr[nk_context], img: nk_image, param2: CString, param3: CInt, alignment: nk_flags): CInt = extern

    def nk_button_text_styled(param0: Ptr[nk_context], param1: Ptr[nk_style_button], title: CString, len: CInt): CInt = extern

    def nk_button_label_styled(param0: Ptr[nk_context], param1: Ptr[nk_style_button], title: CString): CInt = extern

    def nk_button_symbol_styled(param0: Ptr[nk_context], param1: Ptr[nk_style_button], param2: nk_symbol_type): CInt = extern

    def nk_button_image_styled(param0: Ptr[nk_context], param1: Ptr[nk_style_button], img: nk_image): CInt = extern

    def nk_button_symbol_text_styled(param0: Ptr[nk_context], param1: Ptr[nk_style_button], param2: nk_symbol_type, param3: CString, param4: CInt, alignment: nk_flags): CInt = extern

    def nk_button_symbol_label_styled(ctx: Ptr[nk_context], style: Ptr[nk_style_button], symbol: nk_symbol_type, title: CString, align: nk_flags): CInt = extern

    def nk_button_image_label_styled(param0: Ptr[nk_context], param1: Ptr[nk_style_button], img: nk_image, param3: CString, text_alignment: nk_flags): CInt = extern

    def nk_button_image_text_styled(param0: Ptr[nk_context], param1: Ptr[nk_style_button], img: nk_image, param3: CString, param4: CInt, alignment: nk_flags): CInt = extern

    def nk_button_set_behavior(param0: Ptr[nk_context], param1: nk_button_behavior): Unit = extern

    def nk_button_push_behavior(param0: Ptr[nk_context], param1: nk_button_behavior): CInt = extern

    def nk_button_pop_behavior(param0: Ptr[nk_context]): CInt = extern

    def nk_check_label(param0: Ptr[nk_context], param1: CString, active: CInt): CInt = extern

    def nk_check_text(param0: Ptr[nk_context], param1: CString, param2: CInt, active: CInt): CInt = extern

    def nk_check_flags_label(param0: Ptr[nk_context], param1: CString, flags: CUnsignedInt, value: CUnsignedInt): CUnsignedInt = extern

    def nk_check_flags_text(param0: Ptr[nk_context], param1: CString, param2: CInt, flags: CUnsignedInt, value: CUnsignedInt): CUnsignedInt = extern

    def nk_checkbox_label(param0: Ptr[nk_context], param1: CString, active: Ptr[CInt]): CInt = extern

    def nk_checkbox_text(param0: Ptr[nk_context], param1: CString, param2: CInt, active: Ptr[CInt]): CInt = extern

    def nk_checkbox_flags_label(param0: Ptr[nk_context], param1: CString, flags: Ptr[CUnsignedInt], value: CUnsignedInt): CInt = extern

    def nk_checkbox_flags_text(param0: Ptr[nk_context], param1: CString, param2: CInt, flags: Ptr[CUnsignedInt], value: CUnsignedInt): CInt = extern

    def nk_radio_label(param0: Ptr[nk_context], param1: CString, active: Ptr[CInt]): CInt = extern

    def nk_radio_text(param0: Ptr[nk_context], param1: CString, param2: CInt, active: Ptr[CInt]): CInt = extern

    def nk_option_label(param0: Ptr[nk_context], param1: CString, active: CInt): CInt = extern

    def nk_option_text(param0: Ptr[nk_context], param1: CString, param2: CInt, active: CInt): CInt = extern

    def nk_selectable_label(param0: Ptr[nk_context], param1: CString, align: nk_flags, value: Ptr[CInt]): CInt = extern

    def nk_selectable_text(param0: Ptr[nk_context], param1: CString, param2: CInt, align: nk_flags, value: Ptr[CInt]): CInt = extern

    def nk_selectable_image_label(param0: Ptr[nk_context], param1: nk_image, param2: CString, align: nk_flags, value: Ptr[CInt]): CInt = extern

    def nk_selectable_image_text(param0: Ptr[nk_context], param1: nk_image, param2: CString, param3: CInt, align: nk_flags, value: Ptr[CInt]): CInt = extern

    def nk_selectable_symbol_label(param0: Ptr[nk_context], param1: nk_symbol_type, param2: CString, align: nk_flags, value: Ptr[CInt]): CInt = extern

    def nk_selectable_symbol_text(param0: Ptr[nk_context], param1: nk_symbol_type, param2: CString, param3: CInt, align: nk_flags, value: Ptr[CInt]): CInt = extern

    def nk_select_label(param0: Ptr[nk_context], param1: CString, align: nk_flags, value: CInt): CInt = extern

    def nk_select_text(param0: Ptr[nk_context], param1: CString, param2: CInt, align: nk_flags, value: CInt): CInt = extern

    def nk_select_image_label(param0: Ptr[nk_context], param1: nk_image, param2: CString, align: nk_flags, value: CInt): CInt = extern

    def nk_select_image_text(param0: Ptr[nk_context], param1: nk_image, param2: CString, param3: CInt, align: nk_flags, value: CInt): CInt = extern

    def nk_select_symbol_label(param0: Ptr[nk_context], param1: nk_symbol_type, param2: CString, align: nk_flags, value: CInt): CInt = extern

    def nk_select_symbol_text(param0: Ptr[nk_context], param1: nk_symbol_type, param2: CString, param3: CInt, align: nk_flags, value: CInt): CInt = extern

    def nk_slide_float(param0: Ptr[nk_context], min: CFloat, `val`: CFloat, max: CFloat, step: CFloat): CFloat = extern

    def nk_slide_int(param0: Ptr[nk_context], min: CInt, `val`: CInt, max: CInt, step: CInt): CInt = extern

    def nk_slider_float(param0: Ptr[nk_context], min: CFloat, `val`: Ptr[CFloat], max: CFloat, step: CFloat): CInt = extern

    def nk_slider_int(param0: Ptr[nk_context], min: CInt, `val`: Ptr[CInt], max: CInt, step: CInt): CInt = extern

    def nk_progress(param0: Ptr[nk_context], cur: Ptr[nk_size], max: nk_size, modifyable: CInt): CInt = extern

    def nk_prog(param0: Ptr[nk_context], cur: nk_size, max: nk_size, modifyable: CInt): nk_size = extern

    def nk_color_picker(param0: Ptr[nk_context], param1: nk_colorf, param2: nk_color_format): nk_colorf = extern

    def nk_color_pick(param0: Ptr[nk_context], param1: Ptr[nk_colorf], param2: nk_color_format): CInt = extern

    def nk_property_int(param0: Ptr[nk_context], name: CString, min: CInt, `val`: Ptr[CInt], max: CInt, step: CInt, inc_per_pixel: CFloat): Unit = extern

    def nk_property_float(param0: Ptr[nk_context], name: CString, min: CFloat, `val`: Ptr[CFloat], max: CFloat, step: CFloat, inc_per_pixel: CFloat): Unit = extern

    def nk_property_double(param0: Ptr[nk_context], name: CString, min: CDouble, `val`: Ptr[CDouble], max: CDouble, step: CDouble, inc_per_pixel: CFloat): Unit = extern

    def nk_propertyi(param0: Ptr[nk_context], name: CString, min: CInt, `val`: CInt, max: CInt, step: CInt, inc_per_pixel: CFloat): CInt = extern

    def nk_propertyf(param0: Ptr[nk_context], name: CString, min: CFloat, `val`: CFloat, max: CFloat, step: CFloat, inc_per_pixel: CFloat): CFloat = extern

    def nk_propertyd(param0: Ptr[nk_context], name: CString, min: CDouble, `val`: CDouble, max: CDouble, step: CDouble, inc_per_pixel: CFloat): CDouble = extern


    def nk_edit_string(param0: Ptr[nk_context], param1: nk_flags, buffer: CString, len: Ptr[CInt], max: CInt, param5: nk_plugin_filter): nk_flags = extern

    def nk_edit_string_zero_terminated(param0: Ptr[nk_context], param1: nk_flags, buffer: CString, max: CInt, param4: nk_plugin_filter): nk_flags = extern

    def nk_edit_buffer(param0: Ptr[nk_context], param1: nk_flags, param2: Ptr[nk_text_edit], param3: nk_plugin_filter): nk_flags = extern

    def nk_edit_focus(param0: Ptr[nk_context], flags: nk_flags): Unit = extern

    def nk_edit_unfocus(param0: Ptr[nk_context]): Unit = extern

    def nk_chart_begin(param0: Ptr[nk_context], param1: nk_chart_type, num: CInt, min: CFloat, max: CFloat): CInt = extern

    def nk_chart_begin_colored(param0: Ptr[nk_context], param1: nk_chart_type, param2: nk_color, active: nk_color, num: CInt, min: CFloat, max: CFloat): CInt = extern

    def nk_chart_add_slot(ctx: Ptr[nk_context], param1: nk_chart_type, count: CInt, min_value: CFloat, max_value: CFloat): Unit = extern

    def nk_chart_add_slot_colored(ctx: Ptr[nk_context], param1: nk_chart_type, param2: nk_color, active: nk_color, count: CInt, min_value: CFloat, max_value: CFloat): Unit = extern

    def nk_chart_push(param0: Ptr[nk_context], param1: CFloat): nk_flags = extern

    def nk_chart_push_slot(param0: Ptr[nk_context], param1: CFloat, param2: CInt): nk_flags = extern

    def nk_chart_end(param0: Ptr[nk_context]): Unit = extern

    def nk_plot(param0: Ptr[nk_context], param1: nk_chart_type, values: Ptr[CFloat], count: CInt, offset: CInt): Unit = extern

    def nk_plot_function(param0: Ptr[nk_context], param1: nk_chart_type, userdata: Ptr[Byte], value_getter: CFunctionPtr2[Ptr[Byte], CInt, CFloat], count: CInt, offset: CInt): Unit = extern

    def nk_popup_begin(param0: Ptr[nk_context], param1: nk_popup_type, param2: CString, param3: nk_flags, bounds: nk_rect): CInt = extern

    def nk_popup_close(param0: Ptr[nk_context]): Unit = extern

    def nk_popup_end(param0: Ptr[nk_context]): Unit = extern

    def nk_combo(param0: Ptr[nk_context], items: Ptr[CString], count: CInt, selected: CInt, item_height: CInt, size: nk_vec2): CInt = extern

    def nk_combo_separator(param0: Ptr[nk_context], items_separated_by_separator: CString, separator: CInt, selected: CInt, count: CInt, item_height: CInt, size: nk_vec2): CInt = extern

    def nk_combo_string(param0: Ptr[nk_context], items_separated_by_zeros: CString, selected: CInt, count: CInt, item_height: CInt, size: nk_vec2): CInt = extern

    def nk_combo_callback(param0: Ptr[nk_context], item_getter: CFunctionPtr3[Ptr[Byte], CInt, Ptr[CString], Unit], userdata: Ptr[Byte], selected: CInt, count: CInt, item_height: CInt, size: nk_vec2): CInt = extern

    def nk_combobox(param0: Ptr[nk_context], items: Ptr[CString], count: CInt, selected: Ptr[CInt], item_height: CInt, size: nk_vec2): Unit = extern

    def nk_combobox_string(param0: Ptr[nk_context], items_separated_by_zeros: CString, selected: Ptr[CInt], count: CInt, item_height: CInt, size: nk_vec2): Unit = extern

    def nk_combobox_separator(param0: Ptr[nk_context], items_separated_by_separator: CString, separator: CInt, selected: Ptr[CInt], count: CInt, item_height: CInt, size: nk_vec2): Unit = extern

    def nk_combobox_callback(param0: Ptr[nk_context], item_getter: CFunctionPtr3[Ptr[Byte], CInt, Ptr[CString], Unit], param2: Ptr[Byte], selected: Ptr[CInt], count: CInt, item_height: CInt, size: nk_vec2): Unit = extern

    def nk_combo_begin_text(param0: Ptr[nk_context], selected: CString, param2: CInt, size: nk_vec2): CInt = extern

    def nk_combo_begin_label(param0: Ptr[nk_context], selected: CString, size: nk_vec2): CInt = extern

    def nk_combo_begin_color(param0: Ptr[nk_context], color: nk_color, size: nk_vec2): CInt = extern

    def nk_combo_begin_symbol(param0: Ptr[nk_context], param1: nk_symbol_type, size: nk_vec2): CInt = extern

    def nk_combo_begin_symbol_label(param0: Ptr[nk_context], selected: CString, param2: nk_symbol_type, size: nk_vec2): CInt = extern

    def nk_combo_begin_symbol_text(param0: Ptr[nk_context], selected: CString, param2: CInt, param3: nk_symbol_type, size: nk_vec2): CInt = extern

    def nk_combo_begin_image(param0: Ptr[nk_context], img: nk_image, size: nk_vec2): CInt = extern

    def nk_combo_begin_image_label(param0: Ptr[nk_context], selected: CString, param2: nk_image, size: nk_vec2): CInt = extern

    def nk_combo_begin_image_text(param0: Ptr[nk_context], selected: CString, param2: CInt, param3: nk_image, size: nk_vec2): CInt = extern

    def nk_combo_item_label(param0: Ptr[nk_context], param1: CString, alignment: nk_flags): CInt = extern

    def nk_combo_item_text(param0: Ptr[nk_context], param1: CString, param2: CInt, alignment: nk_flags): CInt = extern

    def nk_combo_item_image_label(param0: Ptr[nk_context], param1: nk_image, param2: CString, alignment: nk_flags): CInt = extern

    def nk_combo_item_image_text(param0: Ptr[nk_context], param1: nk_image, param2: CString, param3: CInt, alignment: nk_flags): CInt = extern

    def nk_combo_item_symbol_label(param0: Ptr[nk_context], param1: nk_symbol_type, param2: CString, alignment: nk_flags): CInt = extern

    def nk_combo_item_symbol_text(param0: Ptr[nk_context], param1: nk_symbol_type, param2: CString, param3: CInt, alignment: nk_flags): CInt = extern

    def nk_combo_close(param0: Ptr[nk_context]): Unit = extern

    def nk_combo_end(param0: Ptr[nk_context]): Unit = extern

    def nk_contextual_begin(param0: Ptr[nk_context], param1: nk_flags, param2: nk_vec2, trigger_bounds: nk_rect): CInt = extern

    def nk_contextual_item_text(param0: Ptr[nk_context], param1: CString, param2: CInt, align: nk_flags): CInt = extern

    def nk_contextual_item_label(param0: Ptr[nk_context], param1: CString, align: nk_flags): CInt = extern

    def nk_contextual_item_image_label(param0: Ptr[nk_context], param1: nk_image, param2: CString, alignment: nk_flags): CInt = extern

    def nk_contextual_item_image_text(param0: Ptr[nk_context], param1: nk_image, param2: CString, len: CInt, alignment: nk_flags): CInt = extern

    def nk_contextual_item_symbol_label(param0: Ptr[nk_context], param1: nk_symbol_type, param2: CString, alignment: nk_flags): CInt = extern

    def nk_contextual_item_symbol_text(param0: Ptr[nk_context], param1: nk_symbol_type, param2: CString, param3: CInt, alignment: nk_flags): CInt = extern

    def nk_contextual_close(param0: Ptr[nk_context]): Unit = extern

    def nk_contextual_end(param0: Ptr[nk_context]): Unit = extern

    def nk_tooltip(param0: Ptr[nk_context], param1: CString): Unit = extern

    def nk_tooltip_begin(param0: Ptr[nk_context], width: CFloat): CInt = extern

    def nk_tooltip_end(param0: Ptr[nk_context]): Unit = extern

    def nk_menubar_begin(param0: Ptr[nk_context]): Unit = extern

    def nk_menubar_end(param0: Ptr[nk_context]): Unit = extern

    def nk_menu_begin_text(param0: Ptr[nk_context], title: CString, title_len: CInt, align: nk_flags, size: nk_vec2): CInt = extern

    def nk_menu_begin_label(param0: Ptr[nk_context], param1: CString, align: nk_flags, size: nk_vec2): CInt = extern

    def nk_menu_begin_image(param0: Ptr[nk_context], param1: CString, param2: nk_image, size: nk_vec2): CInt = extern

    def nk_menu_begin_image_text(param0: Ptr[nk_context], param1: CString, param2: CInt, align: nk_flags, param4: nk_image, size: nk_vec2): CInt = extern

    def nk_menu_begin_image_label(param0: Ptr[nk_context], param1: CString, align: nk_flags, param3: nk_image, size: nk_vec2): CInt = extern

    def nk_menu_begin_symbol(param0: Ptr[nk_context], param1: CString, param2: nk_symbol_type, size: nk_vec2): CInt = extern

    def nk_menu_begin_symbol_text(param0: Ptr[nk_context], param1: CString, param2: CInt, align: nk_flags, param4: nk_symbol_type, size: nk_vec2): CInt = extern

    def nk_menu_begin_symbol_label(param0: Ptr[nk_context], param1: CString, align: nk_flags, param3: nk_symbol_type, size: nk_vec2): CInt = extern

    def nk_menu_item_text(param0: Ptr[nk_context], param1: CString, param2: CInt, align: nk_flags): CInt = extern

    def nk_menu_item_label(param0: Ptr[nk_context], param1: CString, alignment: nk_flags): CInt = extern

    def nk_menu_item_image_label(param0: Ptr[nk_context], param1: nk_image, param2: CString, alignment: nk_flags): CInt = extern

    def nk_menu_item_image_text(param0: Ptr[nk_context], param1: nk_image, param2: CString, len: CInt, alignment: nk_flags): CInt = extern

    def nk_menu_item_symbol_text(param0: Ptr[nk_context], param1: nk_symbol_type, param2: CString, param3: CInt, alignment: nk_flags): CInt = extern

    def nk_menu_item_symbol_label(param0: Ptr[nk_context], param1: nk_symbol_type, param2: CString, alignment: nk_flags): CInt = extern

    def nk_menu_close(param0: Ptr[nk_context]): Unit = extern

    def nk_menu_end(param0: Ptr[nk_context]): Unit = extern

    type nk_style_colors = CInt
    type nk_style_cursor = CInt

    def nk_style_default(param0: Ptr[nk_context]): Unit = extern

    def nk_style_from_table(param0: Ptr[nk_context], param1: Ptr[nk_color]): Unit = extern

    def nk_style_load_cursor(param0: Ptr[nk_context], param1: nk_style_cursor, param2: Ptr[nk_cursor]): Unit = extern

    def nk_style_load_all_cursors(param0: Ptr[nk_context], param1: Ptr[nk_cursor]): Unit = extern

    def nk_style_get_color_by_name(param0: nk_style_colors): CString = extern

    def nk_style_set_font(param0: Ptr[nk_context], param1: Ptr[nk_user_font]): Unit = extern

    def nk_style_set_cursor(param0: Ptr[nk_context], param1: nk_style_cursor): CInt = extern

    def nk_style_show_cursor(param0: Ptr[nk_context]): Unit = extern

    def nk_style_hide_cursor(param0: Ptr[nk_context]): Unit = extern

    def nk_style_push_font(param0: Ptr[nk_context], param1: Ptr[nk_user_font]): CInt = extern

    def nk_style_push_float(param0: Ptr[nk_context], param1: Ptr[CFloat], param2: CFloat): CInt = extern

    def nk_style_push_vec2(param0: Ptr[nk_context], param1: Ptr[nk_vec2], param2: nk_vec2): CInt = extern

    def nk_style_push_style_item(param0: Ptr[nk_context], param1: Ptr[nk_style_item], param2: nk_style_item): CInt = extern

    def nk_style_push_flags(param0: Ptr[nk_context], param1: Ptr[nk_flags], param2: nk_flags): CInt = extern

    def nk_style_push_color(param0: Ptr[nk_context], param1: Ptr[nk_color], param2: nk_color): CInt = extern

    def nk_style_pop_font(param0: Ptr[nk_context]): CInt = extern

    def nk_style_pop_float(param0: Ptr[nk_context]): CInt = extern

    def nk_style_pop_vec2(param0: Ptr[nk_context]): CInt = extern

    def nk_style_pop_style_item(param0: Ptr[nk_context]): CInt = extern

    def nk_style_pop_flags(param0: Ptr[nk_context]): CInt = extern

    def nk_style_pop_color(param0: Ptr[nk_context]): CInt = extern

    def nk_rgb(r: CInt, g: CInt, b: CInt): nk_color = extern

    def nk_rgb_iv(rgb: Ptr[CInt]): nk_color = extern

    def nk_rgb_bv(rgb: Ptr[nk_byte]): nk_color = extern

    def nk_rgb_f(r: CFloat, g: CFloat, b: CFloat): nk_color = extern

    def nk_rgb_fv(rgb: Ptr[CFloat]): nk_color = extern

    def nk_rgb_cf(c: nk_colorf): nk_color = extern

    def nk_rgb_hex(rgb: CString): nk_color = extern

    def nk_rgba(r: CInt, g: CInt, b: CInt, a: CInt): nk_color = extern

    def nk_rgba_u32(param0: nk_uint): nk_color = extern

    def nk_rgba_iv(rgba: Ptr[CInt]): nk_color = extern

    def nk_rgba_bv(rgba: Ptr[nk_byte]): nk_color = extern

    def nk_rgba_f(r: CFloat, g: CFloat, b: CFloat, a: CFloat): nk_color = extern

    def nk_rgba_fv(rgba: Ptr[CFloat]): nk_color = extern

    def nk_rgba_cf(c: nk_colorf): nk_color = extern

    def nk_rgba_hex(rgb: CString): nk_color = extern

    def nk_hsva_colorf(h: CFloat, s: CFloat, v: CFloat, a: CFloat): nk_colorf = extern

    def nk_hsva_colorfv(c: Ptr[CFloat]): nk_colorf = extern

    def nk_colorf_hsva_f(out_h: Ptr[CFloat], out_s: Ptr[CFloat], out_v: Ptr[CFloat], out_a: Ptr[CFloat], in: nk_colorf): Unit = extern

    def nk_colorf_hsva_fv(hsva: Ptr[CFloat], in: nk_colorf): Unit = extern

    def nk_hsv(h: CInt, s: CInt, v: CInt): nk_color = extern

    def nk_hsv_iv(hsv: Ptr[CInt]): nk_color = extern

    def nk_hsv_bv(hsv: Ptr[nk_byte]): nk_color = extern

    def nk_hsv_f(h: CFloat, s: CFloat, v: CFloat): nk_color = extern

    def nk_hsv_fv(hsv: Ptr[CFloat]): nk_color = extern

    def nk_hsva(h: CInt, s: CInt, v: CInt, a: CInt): nk_color = extern

    def nk_hsva_iv(hsva: Ptr[CInt]): nk_color = extern

    def nk_hsva_bv(hsva: Ptr[nk_byte]): nk_color = extern

    def nk_hsva_f(h: CFloat, s: CFloat, v: CFloat, a: CFloat): nk_color = extern

    def nk_hsva_fv(hsva: Ptr[CFloat]): nk_color = extern

    def nk_color_f(r: Ptr[CFloat], g: Ptr[CFloat], b: Ptr[CFloat], a: Ptr[CFloat], param4: nk_color): Unit = extern

    def nk_color_fv(rgba_out: Ptr[CFloat], param1: nk_color): Unit = extern

    def nk_color_cf(param0: nk_color): nk_colorf = extern

    def nk_color_d(r: Ptr[CDouble], g: Ptr[CDouble], b: Ptr[CDouble], a: Ptr[CDouble], param4: nk_color): Unit = extern

    def nk_color_dv(rgba_out: Ptr[CDouble], param1: nk_color): Unit = extern

    def nk_color_u32(param0: nk_color): nk_uint = extern

    def nk_color_hex_rgba(output: CString, param1: nk_color): Unit = extern

    def nk_color_hex_rgb(output: CString, param1: nk_color): Unit = extern

    def nk_color_hsv_i(out_h: Ptr[CInt], out_s: Ptr[CInt], out_v: Ptr[CInt], param3: nk_color): Unit = extern

    def nk_color_hsv_b(out_h: Ptr[nk_byte], out_s: Ptr[nk_byte], out_v: Ptr[nk_byte], param3: nk_color): Unit = extern

    def nk_color_hsv_iv(hsv_out: Ptr[CInt], param1: nk_color): Unit = extern

    def nk_color_hsv_bv(hsv_out: Ptr[nk_byte], param1: nk_color): Unit = extern

    def nk_color_hsv_f(out_h: Ptr[CFloat], out_s: Ptr[CFloat], out_v: Ptr[CFloat], param3: nk_color): Unit = extern

    def nk_color_hsv_fv(hsv_out: Ptr[CFloat], param1: nk_color): Unit = extern

    def nk_color_hsva_i(h: Ptr[CInt], s: Ptr[CInt], v: Ptr[CInt], a: Ptr[CInt], param4: nk_color): Unit = extern

    def nk_color_hsva_b(h: Ptr[nk_byte], s: Ptr[nk_byte], v: Ptr[nk_byte], a: Ptr[nk_byte], param4: nk_color): Unit = extern

    def nk_color_hsva_iv(hsva_out: Ptr[CInt], param1: nk_color): Unit = extern

    def nk_color_hsva_bv(hsva_out: Ptr[nk_byte], param1: nk_color): Unit = extern

    def nk_color_hsva_f(out_h: Ptr[CFloat], out_s: Ptr[CFloat], out_v: Ptr[CFloat], out_a: Ptr[CFloat], param4: nk_color): Unit = extern

    def nk_color_hsva_fv(hsva_out: Ptr[CFloat], param1: nk_color): Unit = extern

    def nk_handle_ptr(param0: Ptr[Byte]): nk_handle = extern

    def nk_handle_id(param0: CInt): nk_handle = extern

    def nk_image_handle(param0: nk_handle): nk_image = extern

    def nk_image_ptr(param0: Ptr[Byte]): nk_image = extern

    def nk_image_id(param0: CInt): nk_image = extern

    def nk_image_is_subimage(img: Ptr[nk_image]): CInt = extern

    def nk_subimage_ptr(param0: Ptr[Byte], w: CUnsignedShort, h: CUnsignedShort, sub_region: nk_rect): nk_image = extern

    def nk_subimage_id(param0: CInt, w: CUnsignedShort, h: CUnsignedShort, sub_region: nk_rect): nk_image = extern

    def nk_subimage_handle(param0: nk_handle, w: CUnsignedShort, h: CUnsignedShort, sub_region: nk_rect): nk_image = extern

    def nk_murmur_hash(key: Ptr[Byte], len: CInt, seed: nk_hash): nk_hash = extern

    def nk_triangle_from_direction(result: Ptr[nk_vec2], r: nk_rect, pad_x: CFloat, pad_y: CFloat, param4: nk_heading): Unit = extern

    def nk_vec2(x: CFloat, y: CFloat): nk_vec2 = extern

    def nk_vec2i(x: CInt, y: CInt): nk_vec2 = extern

    def nk_vec2v(xy: Ptr[CFloat]): nk_vec2 = extern

    def nk_vec2iv(xy: Ptr[CInt]): nk_vec2 = extern

    def nk_get_null_rect: nk_rect = extern

    def nk_rect(x: CFloat, y: CFloat, w: CFloat, h: CFloat): nk_rect = extern

    def nk_recti(x: CInt, y: CInt, w: CInt, h: CInt): nk_rect = extern

    def nk_recta(pos: nk_vec2, size: nk_vec2): nk_rect = extern

    def nk_rectv(xywh: Ptr[CFloat]): nk_rect = extern

    def nk_rectiv(xywh: Ptr[CInt]): nk_rect = extern

    def nk_rect_pos(param0: nk_rect): nk_vec2 = extern

    def nk_rect_size(param0: nk_rect): nk_vec2 = extern

    def nk_strlen(str: CString): CInt = extern

    def nk_stricmp(s1: CString, s2: CString): CInt = extern

    def nk_stricmpn(s1: CString, s2: CString, n: CInt): CInt = extern

    def nk_strtoi(str: CString, endptr: Ptr[CString]): CInt = extern

    def nk_strtof(str: CString, endptr: Ptr[CString]): CFloat = extern

    def nk_strtod(str: CString, endptr: Ptr[CString]): CDouble = extern

    def nk_strfilter(text: CString, regexp: CString): CInt = extern

    def nk_strmatch_fuzzy_string(str: CString, pattern: CString, out_score: Ptr[CInt]): CInt = extern

    def nk_strmatch_fuzzy_text(txt: CString, txt_len: CInt, pattern: CString, out_score: Ptr[CInt]): CInt = extern

    def nk_utf_decode(param0: CString, param1: Ptr[nk_rune], param2: CInt): CInt = extern

    def nk_utf_encode(param0: nk_rune, param1: CString, param2: CInt): CInt = extern

    def nk_utf_len(param0: CString, byte_len: CInt): CInt = extern

    def nk_utf_at(buffer: CString, length: CInt, index: CInt, unicode: Ptr[nk_rune], len: Ptr[CInt]): CString = extern

    type nk_user_font_glyph = extern
    type nk_text_width_f = CFunctionPtr4[nk_handle, CFloat, CString, CInt, CFloat]
    type nk_query_font_glyph_f = CFunctionPtr5[nk_handle, CFloat, Ptr[nk_user_font_glyph], nk_rune, nk_rune, Unit]
    type nk_user_font = CStruct3[
      nk_handle,
      CFloat,
      nk_text_width_f
    ]
    type nk_memory_status = CStruct6[
      Ptr[Byte],
      CUnsignedInt,
      nk_size,
      nk_size,
      nk_size,
      nk_size
    ]
    type nk_allocation_type = CInt
    type nk_buffer_marker = CStruct2[
      CInt,
      nk_size
    ]
    type nk_memory = CStruct2[
      Ptr[Byte],
      nk_size
    ]
    type nk_buffer = CStruct9[
      Ptr[nk_buffer_marker],
      nk_allocator,
      nk_allocation_type,
      nk_memory,
      CFloat,
      nk_size,
      nk_size,
      nk_size,
      nk_size
    ]

    def nk_buffer_init(param0: Ptr[nk_buffer], param1: Ptr[nk_allocator], size: nk_size): Unit = extern

    def nk_buffer_init_fixed(param0: Ptr[nk_buffer], memory: Ptr[Byte], size: nk_size): Unit = extern

    def nk_buffer_info(param0: Ptr[nk_memory_status], param1: Ptr[nk_buffer]): Unit = extern

    def nk_buffer_push(param0: Ptr[nk_buffer], `type`: nk_buffer_allocation_type, memory: Ptr[Byte], size: nk_size, align: nk_size): Unit = extern

    def nk_buffer_mark(param0: Ptr[nk_buffer], `type`: nk_buffer_allocation_type): Unit = extern

    def nk_buffer_reset(param0: Ptr[nk_buffer], `type`: nk_buffer_allocation_type): Unit = extern

    def nk_buffer_clear(param0: Ptr[nk_buffer]): Unit = extern

    def nk_buffer_free(param0: Ptr[nk_buffer]): Unit = extern

    def nk_buffer_memory(param0: Ptr[nk_buffer]): Ptr[Byte] = extern

    def nk_buffer_memory_const(param0: Ptr[nk_buffer]): Ptr[Byte] = extern

    def nk_buffer_total(param0: Ptr[nk_buffer]): nk_size = extern

    type nk_str = CStruct2[
      nk_buffer,
      CInt
    ]

    def nk_str_init(param0: Ptr[nk_str], param1: Ptr[nk_allocator], size: nk_size): Unit = extern

    def nk_str_init_fixed(param0: Ptr[nk_str], memory: Ptr[Byte], size: nk_size): Unit = extern

    def nk_str_clear(param0: Ptr[nk_str]): Unit = extern

    def nk_str_free(param0: Ptr[nk_str]): Unit = extern

    def nk_str_append_text_char(param0: Ptr[nk_str], param1: CString, param2: CInt): CInt = extern

    def nk_str_append_str_char(param0: Ptr[nk_str], param1: CString): CInt = extern

    def nk_str_append_text_utf8(param0: Ptr[nk_str], param1: CString, param2: CInt): CInt = extern

    def nk_str_append_str_utf8(param0: Ptr[nk_str], param1: CString): CInt = extern

    def nk_str_append_text_runes(param0: Ptr[nk_str], param1: Ptr[nk_rune], param2: CInt): CInt = extern

    def nk_str_append_str_runes(param0: Ptr[nk_str], param1: Ptr[nk_rune]): CInt = extern

    def nk_str_insert_at_char(param0: Ptr[nk_str], pos: CInt, param2: CString, param3: CInt): CInt = extern

    def nk_str_insert_at_rune(param0: Ptr[nk_str], pos: CInt, param2: CString, param3: CInt): CInt = extern

    def nk_str_insert_text_char(param0: Ptr[nk_str], pos: CInt, param2: CString, param3: CInt): CInt = extern

    def nk_str_insert_str_char(param0: Ptr[nk_str], pos: CInt, param2: CString): CInt = extern

    def nk_str_insert_text_utf8(param0: Ptr[nk_str], pos: CInt, param2: CString, param3: CInt): CInt = extern

    def nk_str_insert_str_utf8(param0: Ptr[nk_str], pos: CInt, param2: CString): CInt = extern

    def nk_str_insert_text_runes(param0: Ptr[nk_str], pos: CInt, param2: Ptr[nk_rune], param3: CInt): CInt = extern

    def nk_str_insert_str_runes(param0: Ptr[nk_str], pos: CInt, param2: Ptr[nk_rune]): CInt = extern

    def nk_str_remove_chars(param0: Ptr[nk_str], len: CInt): Unit = extern

    def nk_str_remove_runes(str: Ptr[nk_str], len: CInt): Unit = extern

    def nk_str_delete_chars(param0: Ptr[nk_str], pos: CInt, len: CInt): Unit = extern

    def nk_str_delete_runes(param0: Ptr[nk_str], pos: CInt, len: CInt): Unit = extern

    def nk_str_at_char(param0: Ptr[nk_str], pos: CInt): CString = extern

    def nk_str_at_rune(param0: Ptr[nk_str], pos: CInt, unicode: Ptr[nk_rune], len: Ptr[CInt]): CString = extern

    def nk_str_rune_at(param0: Ptr[nk_str], pos: CInt): nk_rune = extern

    def nk_str_at_char_const(param0: Ptr[nk_str], pos: CInt): CString = extern

    def nk_str_at_const(param0: Ptr[nk_str], pos: CInt, unicode: Ptr[nk_rune], len: Ptr[CInt]): CString = extern

    def nk_str_get(param0: Ptr[nk_str]): CString = extern

    def nk_str_get_const(param0: Ptr[nk_str]): CString = extern

    def nk_str_len(param0: Ptr[nk_str]): CInt = extern

    def nk_str_len_char(param0: Ptr[nk_str]): CInt = extern

    type nk_text_edit = extern
    type nk_clipboard = CStruct3[
      nk_handle,
      nk_plugin_paste,
      nk_plugin_copy
    ]
    type nk_text_undo_record = CStruct4[
      CInt,
      CShort,
      CShort,
      CShort
    ]
    type nk_text_undo_state = CStruct6[
      Ptr[nk_text_undo_record],
      Ptr[nk_rune],
      CShort,
      CShort,
      CShort,
      CShort
    ]


    type nk_text_edit = CStruct16[
      nk_clipboard,
      nk_str,
      nk_plugin_filter,
      nk_vec2,
      CInt,
      CInt,
      CInt,
      CUnsignedChar,
      CUnsignedChar,
      CUnsignedChar,
      CUnsignedChar,
      CUnsignedChar,
      CUnsignedChar,
      CUnsignedChar,
      CFloat,
      nk_text_undo_state
    ]

    def nk_filter_default(param0: Ptr[nk_text_edit], unicode: nk_rune): CInt = extern

    def nk_filter_ascii(param0: Ptr[nk_text_edit], unicode: nk_rune): CInt = extern

    def nk_filter_float(param0: Ptr[nk_text_edit], unicode: nk_rune): CInt = extern

    def nk_filter_decimal(param0: Ptr[nk_text_edit], unicode: nk_rune): CInt = extern

    def nk_filter_hex(param0: Ptr[nk_text_edit], unicode: nk_rune): CInt = extern

    def nk_filter_oct(param0: Ptr[nk_text_edit], unicode: nk_rune): CInt = extern

    def nk_filter_binary(param0: Ptr[nk_text_edit], unicode: nk_rune): CInt = extern

    def nk_textedit_init(param0: Ptr[nk_text_edit], param1: Ptr[nk_allocator], size: nk_size): Unit = extern

    def nk_textedit_init_fixed(param0: Ptr[nk_text_edit], memory: Ptr[Byte], size: nk_size): Unit = extern

    def nk_textedit_free(param0: Ptr[nk_text_edit]): Unit = extern

    def nk_textedit_text(param0: Ptr[nk_text_edit], param1: CString, total_len: CInt): Unit = extern

    def nk_textedit_delete(param0: Ptr[nk_text_edit], where: CInt, len: CInt): Unit = extern

    def nk_textedit_delete_selection(param0: Ptr[nk_text_edit]): Unit = extern

    def nk_textedit_select_all(param0: Ptr[nk_text_edit]): Unit = extern

    def nk_textedit_cut(param0: Ptr[nk_text_edit]): CInt = extern

    def nk_textedit_paste(param0: Ptr[nk_text_edit], param1: CString, len: CInt): CInt = extern

    def nk_textedit_undo(param0: Ptr[nk_text_edit]): Unit = extern

    def nk_textedit_redo(param0: Ptr[nk_text_edit]): Unit = extern

    type nk_command_type = CInt
    type nk_command = CStruct2[
      nk_command_type,
      nk_size
    ]
    type nk_command_scissor = CStruct5[
      nk_command,
      CShort,
      CShort,
      CUnsignedShort,
      CUnsignedShort
    ]
    type nk_command_line = CStruct5[
      nk_command,
      CUnsignedShort,
      nk_vec2i,
      nk_vec2i,
      nk_color
    ]
    type nk_command_curve = CStruct6[
      nk_command,
      CUnsignedShort,
      nk_vec2i,
      nk_vec2i,
      Ptr[nk_vec2i],
      nk_color
    ]
    type nk_command_rect = CStruct8[
      nk_command,
      CUnsignedShort,
      CUnsignedShort,
      CShort,
      CShort,
      CUnsignedShort,
      CUnsignedShort,
      nk_color
    ]
    type nk_command_rect_filled = CStruct7[
      nk_command,
      CUnsignedShort,
      CShort,
      CShort,
      CUnsignedShort,
      CUnsignedShort,
      nk_color
    ]
    type nk_command_rect_multi_color = CStruct9[
      nk_command,
      CShort,
      CShort,
      CUnsignedShort,
      CUnsignedShort,
      nk_color,
      nk_color,
      nk_color,
      nk_color
    ]
    type nk_command_triangle = CStruct6[
      nk_command,
      CUnsignedShort,
      nk_vec2i,
      nk_vec2i,
      nk_vec2i,
      nk_color
    ]
    type nk_command_triangle_filled = CStruct5[
      nk_command,
      nk_vec2i,
      nk_vec2i,
      nk_vec2i,
      nk_color
    ]
    type nk_command_circle = CStruct7[
      nk_command,
      CShort,
      CShort,
      CUnsignedShort,
      CUnsignedShort,
      CUnsignedShort,
      nk_color
    ]
    type nk_command_circle_filled = CStruct6[
      nk_command,
      CShort,
      CShort,
      CUnsignedShort,
      CUnsignedShort,
      nk_color
    ]
    type nk_command_arc = CStruct7[
      nk_command,
      CShort,
      CShort,
      CUnsignedShort,
      CUnsignedShort,
      Ptr[CFloat],
      nk_color
    ]
    type nk_command_arc_filled = CStruct6[
      nk_command,
      CShort,
      CShort,
      CUnsignedShort,
      Ptr[CFloat],
      nk_color
    ]
    type nk_command_polygon = CStruct5[
      nk_command,
      nk_color,
      CUnsignedShort,
      CUnsignedShort,
      Ptr[nk_vec2i]
    ]
    type nk_command_polygon_filled = CStruct4[
      nk_command,
      nk_color,
      CUnsignedShort,
      Ptr[nk_vec2i]
    ]
    type nk_command_polyline = CStruct5[
      nk_command,
      nk_color,
      CUnsignedShort,
      CUnsignedShort,
      Ptr[nk_vec2i]
    ]
    type nk_command_image = CStruct7[
      nk_command,
      CShort,
      CShort,
      CUnsignedShort,
      CUnsignedShort,
      nk_image,
      nk_color
    ]
    type nk_command_custom_callback = CFunctionPtr6[Ptr[Byte], CShort, CShort, CUnsignedShort, CUnsignedShort, nk_handle, Unit]
    type nk_command_custom = CStruct7[
      nk_command,
      CShort,
      CShort,
      CUnsignedShort,
      CUnsignedShort,
      nk_handle,
      nk_command_custom_callback
    ]
    type nk_command_text = CStruct11[
      nk_command,
      Ptr[nk_user_font],
      nk_color,
      nk_color,
      CShort,
      CShort,
      CUnsignedShort,
      CUnsignedShort,
      CFloat,
      CInt,
      CString
    ]

    type nk_command_buffer = CStruct7[
      Ptr[nk_buffer],
      nk_rect,
      CInt,
      nk_handle,
      nk_size,
      nk_size,
      nk_size
    ]

    def nk_stroke_line(b: Ptr[nk_command_buffer], x0: CFloat, y0: CFloat, x1: CFloat, y1: CFloat, line_thickness: CFloat, param6: nk_color): Unit = extern

    def nk_stroke_curve(param0: Ptr[nk_command_buffer], param1: CFloat, param2: CFloat, param3: CFloat, param4: CFloat, param5: CFloat, param6: CFloat, param7: CFloat, param8: CFloat, line_thickness: CFloat, param10: nk_color): Unit = extern

    def nk_stroke_rect(param0: Ptr[nk_command_buffer], param1: nk_rect, rounding: CFloat, line_thickness: CFloat, param4: nk_color): Unit = extern

    def nk_stroke_circle(param0: Ptr[nk_command_buffer], param1: nk_rect, line_thickness: CFloat, param3: nk_color): Unit = extern

    def nk_stroke_arc(param0: Ptr[nk_command_buffer], cx: CFloat, cy: CFloat, radius: CFloat, a_min: CFloat, a_max: CFloat, line_thickness: CFloat, param7: nk_color): Unit = extern

    def nk_stroke_triangle(param0: Ptr[nk_command_buffer], param1: CFloat, param2: CFloat, param3: CFloat, param4: CFloat, param5: CFloat, param6: CFloat, line_thichness: CFloat, param8: nk_color): Unit = extern

    def nk_stroke_polyline(param0: Ptr[nk_command_buffer], points: Ptr[CFloat], point_count: CInt, line_thickness: CFloat, col: nk_color): Unit = extern

    def nk_stroke_polygon(param0: Ptr[nk_command_buffer], param1: Ptr[CFloat], point_count: CInt, line_thickness: CFloat, param4: nk_color): Unit = extern

    def nk_fill_rect(param0: Ptr[nk_command_buffer], param1: nk_rect, rounding: CFloat, param3: nk_color): Unit = extern

    def nk_fill_rect_multi_color(param0: Ptr[nk_command_buffer], param1: nk_rect, left: nk_color, top: nk_color, right: nk_color, bottom: nk_color): Unit = extern

    def nk_fill_circle(param0: Ptr[nk_command_buffer], param1: nk_rect, param2: nk_color): Unit = extern

    def nk_fill_arc(param0: Ptr[nk_command_buffer], cx: CFloat, cy: CFloat, radius: CFloat, a_min: CFloat, a_max: CFloat, param6: nk_color): Unit = extern

    def nk_fill_triangle(param0: Ptr[nk_command_buffer], x0: CFloat, y0: CFloat, x1: CFloat, y1: CFloat, x2: CFloat, y2: CFloat, param7: nk_color): Unit = extern

    def nk_fill_polygon(param0: Ptr[nk_command_buffer], param1: Ptr[CFloat], point_count: CInt, param3: nk_color): Unit = extern

    def nk_draw_image(param0: Ptr[nk_command_buffer], param1: nk_rect, param2: Ptr[nk_image], param3: nk_color): Unit = extern

    def nk_draw_text(param0: Ptr[nk_command_buffer], param1: nk_rect, text: CString, len: CInt, param4: Ptr[nk_user_font], param5: nk_color, param6: nk_color): Unit = extern

    def nk_push_scissor(param0: Ptr[nk_command_buffer], param1: nk_rect): Unit = extern

    def nk_push_custom(param0: Ptr[nk_command_buffer], param1: nk_rect, param2: nk_command_custom_callback, usr: nk_handle): Unit = extern

    type nk_mouse_button = CStruct3[
      CInt,
      CUnsignedInt,
      nk_vec2
    ]
    type nk_mouse = CStruct8[
      Ptr[nk_mouse_button],
      nk_vec2,
      nk_vec2,
      nk_vec2,
      nk_vec2,
      CUnsignedChar,
      CUnsignedChar,
      CUnsignedChar
    ]
    type nk_key = CStruct2[
      CInt,
      CUnsignedInt
    ]
    type nk_keyboard = CStruct3[
      Ptr[nk_key],
      CString,
      CInt
    ]
    type nk_input = CStruct2[
      nk_keyboard,
      nk_mouse
    ]

    def nk_input_has_mouse_click(param0: Ptr[nk_input], param1: nk_buttons): CInt = extern

    def nk_input_has_mouse_click_in_rect(param0: Ptr[nk_input], param1: nk_buttons, param2: nk_rect): CInt = extern

    def nk_input_has_mouse_click_down_in_rect(param0: Ptr[nk_input], param1: nk_buttons, param2: nk_rect, down: CInt): CInt = extern

    def nk_input_is_mouse_click_in_rect(param0: Ptr[nk_input], param1: nk_buttons, param2: nk_rect): CInt = extern

    def nk_input_is_mouse_click_down_in_rect(i: Ptr[nk_input], id: nk_buttons, b: nk_rect, down: CInt): CInt = extern

    def nk_input_any_mouse_click_in_rect(param0: Ptr[nk_input], param1: nk_rect): CInt = extern

    def nk_input_is_mouse_prev_hovering_rect(param0: Ptr[nk_input], param1: nk_rect): CInt = extern

    def nk_input_is_mouse_hovering_rect(param0: Ptr[nk_input], param1: nk_rect): CInt = extern

    def nk_input_mouse_clicked(param0: Ptr[nk_input], param1: nk_buttons, param2: nk_rect): CInt = extern

    def nk_input_is_mouse_down(param0: Ptr[nk_input], param1: nk_buttons): CInt = extern

    def nk_input_is_mouse_pressed(param0: Ptr[nk_input], param1: nk_buttons): CInt = extern

    def nk_input_is_mouse_released(param0: Ptr[nk_input], param1: nk_buttons): CInt = extern

    def nk_input_is_key_pressed(param0: Ptr[nk_input], param1: nk_keys): CInt = extern

    def nk_input_is_key_released(param0: Ptr[nk_input], param1: nk_keys): CInt = extern

    def nk_input_is_key_down(param0: Ptr[nk_input], param1: nk_keys): CInt = extern

    type nk_style_text = CStruct2[
      nk_color,
      nk_vec2
    ]
    type nk_style_button = CStruct17[
      nk_style_item,
      nk_style_item,
      nk_style_item,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_flags,
      CFloat,
      CFloat,
      nk_vec2,
      nk_vec2,
      nk_vec2,
      nk_handle,
      CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit],
      CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]
    ]
    type nk_style_toggle = CStruct18[
      nk_style_item,
      nk_style_item,
      nk_style_item,
      nk_color,
      nk_style_item,
      nk_style_item,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_flags,
      nk_vec2,
      nk_vec2,
      CFloat,
      CFloat,
      nk_handle,
      CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit],
      CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]
    ]
    type nk_style_selectable = CStruct21[
      nk_style_item,
      nk_style_item,
      nk_style_item,
      nk_style_item,
      nk_style_item,
      nk_style_item,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_flags,
      CFloat,
      nk_vec2,
      nk_vec2,
      nk_vec2,
      nk_handle,
      CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit],
      CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]
    ]
    type nk_style_slider = CStruct25[
      nk_style_item,
      nk_style_item,
      nk_style_item,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_style_item,
      nk_style_item,
      nk_style_item,
      CFloat,
      CFloat,
      CFloat,
      nk_vec2,
      nk_vec2,
      nk_vec2,
      CInt,
      nk_style_button,
      nk_style_button,
      nk_symbol_type,
      nk_symbol_type,
      nk_handle,
      CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit],
      CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]
    ]
    type nk_style_progress = CStruct16[
      nk_style_item,
      nk_style_item,
      nk_style_item,
      nk_color,
      nk_style_item,
      nk_style_item,
      nk_style_item,
      nk_color,
      CFloat,
      CFloat,
      CFloat,
      CFloat,
      nk_vec2,
      nk_handle,
      CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit],
      CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]
    ]
    type nk_style_scrollbar = CStruct21[
      nk_style_item,
      nk_style_item,
      nk_style_item,
      nk_color,
      nk_style_item,
      nk_style_item,
      nk_style_item,
      nk_color,
      CFloat,
      CFloat,
      CFloat,
      CFloat,
      nk_vec2,
      CInt,
      nk_style_button,
      nk_style_button,
      nk_symbol_type,
      nk_symbol_type,
      nk_handle,
      CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit],
      CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]
    ]
    type nk_style_edit = CStruct22[
      nk_style_item,
      nk_style_item,
      nk_style_item,
      nk_color,
      nk_style_scrollbar,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      CFloat,
      CFloat,
      CFloat,
      nk_vec2,
      nk_vec2,
      CFloat
    ]
    type nk_style_property = CStruct18[
      nk_style_item,
      nk_style_item,
      nk_style_item,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_symbol_type,
      nk_symbol_type,
      CFloat,
      CFloat,
      nk_vec2,
      nk_style_edit,
      nk_style_button,
      nk_style_button,
      nk_handle,
      CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit],
      CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]
    ]
    type nk_style_chart = CStruct7[
      nk_style_item,
      nk_color,
      nk_color,
      nk_color,
      CFloat,
      CFloat,
      nk_vec2
    ]
    type nk_style_combo = CStruct19[
      nk_style_item,
      nk_style_item,
      nk_style_item,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_style_button,
      nk_symbol_type,
      nk_symbol_type,
      nk_symbol_type,
      CFloat,
      CFloat,
      nk_vec2,
      nk_vec2,
      nk_vec2
    ]
    type nk_style_tab = CStruct14[
      nk_style_item,
      nk_color,
      nk_color,
      nk_style_button,
      nk_style_button,
      nk_style_button,
      nk_style_button,
      nk_symbol_type,
      nk_symbol_type,
      CFloat,
      CFloat,
      CFloat,
      nk_vec2,
      nk_vec2
    ]

    type nk_style_window_header = CStruct15[
      nk_style_item,
      nk_style_item,
      nk_style_item,
      nk_style_button,
      nk_style_button,
      nk_symbol_type,
      nk_symbol_type,
      nk_symbol_type,
      nk_color,
      nk_color,
      nk_color,
      nk_style_header_align,
      nk_vec2,
      nk_vec2,
      nk_vec2
    ]
    type nk_style_window = CStruct30[
      nk_style_window_header,
      nk_style_item,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_color,
      nk_style_item,
      CFloat,
      CFloat,
      CFloat,
      CFloat,
      CFloat,
      CFloat,
      CFloat,
      CFloat,
      CFloat,
      nk_vec2,
      nk_vec2,
      nk_vec2,
      nk_vec2,
      nk_vec2,
      nk_vec2,
      nk_vec2,
      nk_vec2,
      nk_vec2,
      nk_vec2
    ]
    type nk_style = CStruct22[
      Ptr[nk_user_font],
      Ptr[Ptr[nk_cursor]],
      Ptr[nk_cursor],
      Ptr[nk_cursor],
      CInt,
      nk_style_text,
      nk_style_button,
      nk_style_button,
      nk_style_button,
      nk_style_toggle,
      nk_style_toggle,
      nk_style_selectable,
      nk_style_slider,
      nk_style_progress,
      nk_style_property,
      nk_style_edit,
      nk_style_chart,
      nk_style_scrollbar,
      nk_style_scrollbar,
      nk_style_tab,
      nk_style_combo,
      nk_style_window
    ]

    def nk_style_item_image(img: nk_image): nk_style_item = extern

    def nk_style_item_color(param0: nk_color): nk_style_item = extern

    def nk_style_item_hide: nk_style_item = extern


    type nk_chart_slot = CStruct9[
      nk_chart_type,
      nk_color,
      nk_color,
      CFloat,
      CFloat,
      CFloat,
      CInt,
      nk_vec2,
      CInt
    ]
    type nk_chart = CStruct6[
      CInt,
      CFloat,
      CFloat,
      CFloat,
      CFloat,
      Ptr[nk_chart_slot]
    ]

    type nk_row_layout = CStruct13[
      nk_panel_row_layout_type,
      CInt,
      CFloat,
      CFloat,
      CInt,
      Ptr[CFloat],
      CFloat,
      CFloat,
      CFloat,
      CFloat,
      nk_rect,
      CInt,
      Ptr[CFloat]
    ]
    type nk_popup_buffer = CStruct5[
      nk_size,
      nk_size,
      nk_size,
      nk_size,
      CInt
    ]
    type nk_menu_state = CStruct5[
      CFloat,
      CFloat,
      CFloat,
      CFloat,
      nk_scroll
    ]
    type nk_panel = CStruct18[
      nk_panel_type,
      nk_flags,
      nk_rect,
      Ptr[nk_uint],
      Ptr[nk_uint],
      CFloat,
      CFloat,
      CFloat,
      CFloat,
      CFloat,
      CFloat,
      CUnsignedInt,
      nk_rect,
      nk_menu_state,
      nk_row_layout,
      nk_chart,
      Ptr[nk_command_buffer],
      Ptr[nk_panel]
    ]
    type nk_table = extern

    type nk_popup_state = CStruct10[
      Ptr[nk_window],
      nk_panel_type,
      nk_popup_buffer,
      nk_hash,
      CInt,
      CUnsignedInt,
      CUnsignedInt,
      CUnsignedInt,
      CUnsignedInt,
      nk_rect
    ]
    type nk_edit_state = CStruct11[
      nk_hash,
      CUnsignedInt,
      CUnsignedInt,
      CInt,
      CInt,
      CInt,
      CInt,
      CInt,
      nk_scroll,
      CUnsignedChar,
      CUnsignedChar
    ]
    type nk_property_state = CStruct11[
      CInt,
      CInt,
      CString,
      CInt,
      CInt,
      CInt,
      CInt,
      nk_hash,
      CUnsignedInt,
      CUnsignedInt,
      CInt
    ]
    type nk_window = CStruct18[
      CUnsignedInt,
      nk_hash,
      CString,
      nk_flags,
      nk_rect,
      nk_scroll,
      nk_command_buffer,
      Ptr[nk_panel],
      CFloat,
      nk_property_state,
      nk_popup_state,
      nk_edit_state,
      CUnsignedInt,
      Ptr[nk_table],
      CUnsignedInt,
      Ptr[nk_window],
      Ptr[nk_window],
      Ptr[nk_window]
    ]
    type nk_config_stack_style_item_element = CStruct2[
      Ptr[nk_style_item],
      nk_style_item
    ]
    type nk_config_stack_float_element = CStruct2[
      Ptr[CFloat],
      CFloat
    ]
    type nk_config_stack_vec2_element = CStruct2[
      Ptr[nk_vec2],
      nk_vec2
    ]
    type nk_config_stack_flags_element = CStruct2[
      Ptr[nk_flags],
      nk_flags
    ]
    type nk_config_stack_color_element = CStruct2[
      Ptr[nk_color],
      nk_color
    ]
    type nk_config_stack_user_font_element = CStruct2[
      Ptr[Ptr[nk_user_font]],
      Ptr[nk_user_font]
    ]
    type nk_config_stack_button_behavior_element = CStruct2[
      Ptr[nk_button_behavior],
      nk_button_behavior
    ]
    type nk_config_stack_style_item = CStruct2[
      CInt,
      Ptr[nk_config_stack_style_item_element]
    ]
    type nk_config_stack_float = CStruct2[
      CInt,
      Ptr[nk_config_stack_float_element]
    ]
    type nk_config_stack_vec2 = CStruct2[
      CInt,
      Ptr[nk_config_stack_vec2_element]
    ]
    type nk_config_stack_flags = CStruct2[
      CInt,
      Ptr[nk_config_stack_flags_element]
    ]
    type nk_config_stack_color = CStruct2[
      CInt,
      Ptr[nk_config_stack_color_element]
    ]
    type nk_config_stack_user_font = CStruct2[
      CInt,
      Ptr[nk_config_stack_user_font_element]
    ]
    type nk_config_stack_button_behavior = CStruct2[
      CInt,
      Ptr[nk_config_stack_button_behavior_element]
    ]
    type nk_configuration_stacks = CStruct7[
      nk_config_stack_style_item,
      nk_config_stack_float,
      nk_config_stack_vec2,
      nk_config_stack_flags,
      nk_config_stack_color,
      nk_config_stack_user_font,
      nk_config_stack_button_behavior
    ]
    type nk_table = CStruct6[
      CUnsignedInt,
      CUnsignedInt,
      Ptr[nk_hash],
      Ptr[nk_uint],
      Ptr[nk_table],
      Ptr[nk_table]
    ]
    type nk_page = CStruct3[
      CUnsignedInt,
      Ptr[nk_page],
      Ptr[nk_page_element]
    ]
    type nk_pool = CStruct8[
      nk_allocator,
      nk_allocation_type,
      CUnsignedInt,
      Ptr[nk_page],
      Ptr[nk_page_element],
      CUnsignedInt,
      nk_size,
      nk_size
    ]
    type nk_context = CStruct20[
      nk_input,
      nk_style,
      nk_buffer,
      nk_clipboard,
      nk_flags,
      nk_button_behavior,
      nk_configuration_stacks,
      CFloat,
      nk_text_edit,
      nk_command_buffer,
      CInt,
      CInt,
      nk_pool,
      Ptr[nk_window],
      Ptr[nk_window],
      Ptr[nk_window],
      Ptr[nk_window],
      Ptr[nk_page_element],
      CUnsignedInt,
      CUnsignedInt
    ]
  }

  object nuklearOps {

    import nuklear._

    object nk_ {
      val nk_false = 0
      val nk_true = 1
    }

    implicit class nk_colorOps(val ptr: Ptr[nk_color]) extends AnyVal {
      def r: nk_byte = !ptr._1

      def g: nk_byte = !ptr._2

      def b: nk_byte = !ptr._3

      def a: nk_byte = !ptr._4

      def r_=(v: nk_byte): Unit = !ptr._1 = v

      def g_=(v: nk_byte): Unit = !ptr._2 = v

      def b_=(v: nk_byte): Unit = !ptr._3 = v

      def a_=(v: nk_byte): Unit = !ptr._4 = v
    }

    implicit class nk_colorfOps(val ptr: Ptr[nk_colorf]) extends AnyVal {
      def r: CFloat = !ptr._1

      def g: CFloat = !ptr._2

      def b: CFloat = !ptr._3

      def a: CFloat = !ptr._4

      def r_=(v: CFloat): Unit = !ptr._1 = v

      def g_=(v: CFloat): Unit = !ptr._2 = v

      def b_=(v: CFloat): Unit = !ptr._3 = v

      def a_=(v: CFloat): Unit = !ptr._4 = v
    }

    implicit class nk_vec2Ops(val ptr: Ptr[nk_vec2]) extends AnyVal {
      def x: CFloat = !ptr._1

      def y: CFloat = !ptr._2

      def x_=(v: CFloat): Unit = !ptr._1 = v

      def y_=(v: CFloat): Unit = !ptr._2 = v
    }

    implicit class nk_vec2iOps(val ptr: Ptr[nk_vec2i]) extends AnyVal {
      def x: CShort = !ptr._1

      def y: CShort = !ptr._2

      def x_=(v: CShort): Unit = !ptr._1 = v

      def y_=(v: CShort): Unit = !ptr._2 = v
    }

    implicit class nk_rectOps(val ptr: Ptr[nk_rect]) extends AnyVal {
      def x: CFloat = !ptr._1

      def y: CFloat = !ptr._2

      def w: CFloat = !ptr._3

      def h: CFloat = !ptr._4

      def x_=(v: CFloat): Unit = !ptr._1 = v

      def y_=(v: CFloat): Unit = !ptr._2 = v

      def w_=(v: CFloat): Unit = !ptr._3 = v

      def h_=(v: CFloat): Unit = !ptr._4 = v
    }

    implicit class nk_rectiOps(val ptr: Ptr[nk_recti]) extends AnyVal {
      def x: CShort = !ptr._1

      def y: CShort = !ptr._2

      def w: CShort = !ptr._3

      def h: CShort = !ptr._4

      def x_=(v: CShort): Unit = !ptr._1 = v

      def y_=(v: CShort): Unit = !ptr._2 = v

      def w_=(v: CShort): Unit = !ptr._3 = v

      def h_=(v: CShort): Unit = !ptr._4 = v
    }

    implicit class nk_imageOps(val ptr: Ptr[nk_image]) extends AnyVal {
      def handle: nk_handle = !ptr._1

      def w: CUnsignedShort = !ptr._2

      def h: CUnsignedShort = !ptr._3

      def region: Ptr[CUnsignedShort] = !ptr._4

      def handle_=(v: nk_handle): Unit = !ptr._1 = v

      def w_=(v: CUnsignedShort): Unit = !ptr._2 = v

      def h_=(v: CUnsignedShort): Unit = !ptr._3 = v

      def region_=(v: Ptr[CUnsignedShort]): Unit = !ptr._4 = v
    }

    implicit class nk_cursorOps(val ptr: Ptr[nk_cursor]) extends AnyVal {
      def img: nk_image = !ptr._1

      def size: nk_vec2 = !ptr._2

      def offset: nk_vec2 = !ptr._3

      def img_=(v: nk_image): Unit = !ptr._1 = v

      def size_=(v: nk_vec2): Unit = !ptr._2 = v

      def offset_=(v: nk_vec2): Unit = !ptr._3 = v
    }

    implicit class nk_scrollOps(val ptr: Ptr[nk_scroll]) extends AnyVal {
      def x: nk_uint = !ptr._1

      def y: nk_uint = !ptr._2

      def x_=(v: nk_uint): Unit = !ptr._1 = v

      def y_=(v: nk_uint): Unit = !ptr._2 = v
    }

    object nk_heading {
      val NK_UP = 0
      val NK_RIGHT = 1
      val NK_DOWN = 2
      val NK_LEFT = 3
    }

    object nk_button_behavior {
      val NK_BUTTON_DEFAULT = 0
      val NK_BUTTON_REPEATER = 1
    }

    object nk_modify {
      nk_modify
      nk_modify
    }

    object nk_orientation {
      nk_orientation
      nk_orientation
    }

    object nk_collapse_states {
      val NK_MINIMIZED = false
      val NK_MAXIMIZED = true
    }

    object nk_show_states {
      val NK_HIDDEN = false
      val NK_SHOWN = true
    }

    object nk_chart_type {
      val NK_CHART_LINES = 0
      val NK_CHART_COLUMN = 1
      val NK_CHART_MAX = 2
    }

    object nk_chart_event {
      nk_chart_event
      nk_chart_event
    }

    object nk_color_format {
      val NK_RGB = 0
      val NK_RGBA = 1
    }

    object nk_popup_type {
      val NK_POPUP_STATIC = 0
      val NK_POPUP_DYNAMIC = 1
    }

    object nk_layout_format {
      val NK_DYNAMIC = 0
      val NK_STATIC = 1
    }

    object nk_tree_type {
      val NK_TREE_NODE = 0
      val NK_TREE_TAB = 1
    }

    implicit class nk_allocatorOps(val ptr: Ptr[nk_allocator]) extends AnyVal {
      def userdata: nk_handle = !ptr._1

      def alloc: nk_plugin_alloc = !ptr._2

      def free: nk_plugin_free = !ptr._3

      def userdata_=(v: nk_handle): Unit = !ptr._1 = v

      def alloc_=(v: nk_plugin_alloc): Unit = !ptr._2 = v

      def free_=(v: nk_plugin_free): Unit = !ptr._3 = v
    }

    object nk_symbol_type {
      val NK_SYMBOL_NONE = 0
      val NK_SYMBOL_X = 1
      val NK_SYMBOL_UNDERSCORE = 2
      val NK_SYMBOL_CIRCLE_SOLID = 3
      val NK_SYMBOL_CIRCLE_OUTLINE = 4
      val NK_SYMBOL_RECT_SOLID = 5
      val NK_SYMBOL_RECT_OUTLINE = 6
      val NK_SYMBOL_TRIANGLE_UP = 7
      val NK_SYMBOL_TRIANGLE_DOWN = 8
      val NK_SYMBOL_TRIANGLE_LEFT = 9
      val NK_SYMBOL_TRIANGLE_RIGHT = 10
      val NK_SYMBOL_PLUS = 11
      val NK_SYMBOL_MINUS = 12
      val NK_SYMBOL_MAX = 13
    }

    object nk_keys {
      val NK_KEY_NONE = 0
      val NK_KEY_SHIFT = 1
      val NK_KEY_CTRL = 2
      val NK_KEY_DEL = 3
      val NK_KEY_ENTER = 4
      val NK_KEY_TAB = 5
      val NK_KEY_BACKSPACE = 6
      val NK_KEY_COPY = 7
      val NK_KEY_CUT = 8
      val NK_KEY_PASTE = 9
      val NK_KEY_UP = 10
      val NK_KEY_DOWN = 11
      val NK_KEY_LEFT = 12
      val NK_KEY_RIGHT = 13
      val NK_KEY_TEXT_INSERT_MODE = 14
      val NK_KEY_TEXT_REPLACE_MODE = 15
      val NK_KEY_TEXT_RESET_MODE = 16
      val NK_KEY_TEXT_LINE_START = 17
      val NK_KEY_TEXT_LINE_END = 18
      val NK_KEY_TEXT_START = 19
      val NK_KEY_TEXT_END = 20
      val NK_KEY_TEXT_UNDO = 21
      val NK_KEY_TEXT_REDO = 22
      val NK_KEY_TEXT_SELECT_ALL =23
      val NK_KEY_TEXT_WORD_LEFT = 24
      val NK_KEY_TEXT_WORD_RIGHT = 25
      val NK_KEY_SCROLL_START = 26
      val NK_KEY_SCROLL_END = 27
      val NK_KEY_SCROLL_DOWN = 28
      val NK_KEY_SCROLL_UP = 29
      val NK_KEY_MAX = 30
    }

    object nk_buttons {
      val NK_BUTTON_LEFT = 0
      val NK_BUTTON_MIDDLE = 1
      val NK_BUTTON_RIGHT = 2
      val NK_BUTTON_DOUBLE = 3
      val NK_BUTTON_MAX = 4
    }

    object nk_anti_aliasing {
      val NK_ANTI_ALIASING_OFF = 0
      val NK_ANTI_ALIASING_ON = 1
    }

    object nk_convert_result {
      nk_convert_result
      nk_convert_result
      nk_convert_result
      nk_convert_result
      nk_convert_result
    }

    implicit class nk_draw_null_textureOps(val ptr: Ptr[nk_draw_null_texture]) extends AnyVal {
      def texture: nk_handle = !ptr._1

      def uv: nk_vec2 = !ptr._2

      def texture_=(v: nk_handle): Unit = !ptr._1 = v

      def uv_=(v: nk_vec2): Unit = !ptr._2 = v
    }

    implicit class nk_convert_configOps(val ptr: Ptr[nk_convert_config]) extends AnyVal {
      def global_alpha: CFloat = !ptr._1

      def line_AA: nk_anti_aliasing = !ptr._2

      def shape_AA: nk_anti_aliasing = !ptr._3

      def circle_segment_count: CUnsignedInt = !ptr._4

      def arc_segment_count: CUnsignedInt = !ptr._5

      def curve_segment_count: CUnsignedInt = !ptr._6

      def `null`: nk_draw_null_texture = !ptr._7

      def vertex_layout: Ptr[nk_draw_vertex_layout_element] = !ptr._8

      def vertex_size: nk_size = !ptr._9

      def vertex_alignment: nk_size = !ptr._10

      def global_alpha_=(v: CFloat): Unit = !ptr._1 = v

      def line_AA_=(v: nk_anti_aliasing): Unit = !ptr._2 = v

      def shape_AA_=(v: nk_anti_aliasing): Unit = !ptr._3 = v

      def circle_segment_count_=(v: CUnsignedInt): Unit = !ptr._4 = v

      def arc_segment_count_=(v: CUnsignedInt): Unit = !ptr._5 = v

      def curve_segment_count_=(v: CUnsignedInt): Unit = !ptr._6 = v

      def null_=(v: nk_draw_null_texture): Unit = !ptr._7 = v

      def vertex_layout_=(v: Ptr[nk_draw_vertex_layout_element]): Unit = !ptr._8 = v

      def vertex_size_=(v: nk_size): Unit = !ptr._9 = v

      def vertex_alignment_=(v: nk_size): Unit = !ptr._10 = v
    }

    object nk_panel_flags {
      nk_panel_flags
      nk_panel_flags
      nk_panel_flags
      nk_panel_flags
      nk_panel_flags
      nk_panel_flags
      nk_panel_flags
      nk_panel_flags
      nk_panel_flags
      nk_panel_flags
      nk_panel_flags
    }

    implicit class nk_list_viewOps(val ptr: Ptr[nk_list_view]) extends AnyVal {
      def begin: CInt = !ptr._1

      def end: CInt = !ptr._2

      def count: CInt = !ptr._3

      def total_height: CInt = !ptr._4

      def ctx: Ptr[nk_context] = !ptr._5

      def scroll_pointer: Ptr[nk_uint] = !ptr._6

      def scroll_value: nk_uint = !ptr._7

      def begin_=(v: CInt): Unit = !ptr._1 = v

      def end_=(v: CInt): Unit = !ptr._2 = v

      def count_=(v: CInt): Unit = !ptr._3 = v

      def total_height_=(v: CInt): Unit = !ptr._4 = v

      def ctx_=(v: Ptr[nk_context]): Unit = !ptr._5 = v

      def scroll_pointer_=(v: Ptr[nk_uint]): Unit = !ptr._6 = v

      def scroll_value_=(v: nk_uint): Unit = !ptr._7 = v
    }

    object nk_widget_layout_states {
      val NK_WIDGET_INVALID = 0
      val NK_WIDGET_VALID = 1
      val NK_WIDGET_ROM = 2
    }

    object nk_widget_states {
      nk_widget_states
      nk_widget_states
      nk_widget_states
      nk_widget_states
      nk_widget_states
      nk_widget_states
      nk_widget_states
      nk_widget_states
    }

    object nk_text_align {
      nk_text_align
      nk_text_align
      nk_text_align
      nk_text_align
      nk_text_align
      nk_text_align
    }

    object nk_text_alignment {
      nk_text_alignment
      nk_text_alignment
      nk_text_alignment
    }

    object nk_edit_flags {
      nk_edit_flags
      nk_edit_flags
      nk_edit_flags
      nk_edit_flags
      nk_edit_flags
      nk_edit_flags
      nk_edit_flags
      nk_edit_flags
      nk_edit_flags
      nk_edit_flags
      nk_edit_flags
      nk_edit_flags
      nk_edit_flags
    }

    object nk_edit_types {
      nk_edit_types
      nk_edit_types
      nk_edit_types
      nk_edit_types
    }

    object nk_edit_events {
      nk_edit_events
      nk_edit_events
      nk_edit_events
      nk_edit_events
      nk_edit_events
    }

    object nk_style_colors {
      val NK_COLOR_TEXT = 0
      val NK_COLOR_WINDOW = 1
      val NK_COLOR_HEADER = 2
      val NK_COLOR_BORDER = 3
      val NK_COLOR_BUTTON = 4
      val NK_COLOR_BUTTON_HOVER = 5
      val NK_COLOR_BUTTON_ACTIVE = 6
      val NK_COLOR_TOGGLE = 7
      val NK_COLOR_TOGGLE_HOVER = 8
      val NK_COLOR_TOGGLE_CURSOR = 9
      val NK_COLOR_SELECT = 10
      val NK_COLOR_SELECT_ACTIVE = 11
      val NK_COLOR_SLIDER = 12
      val NK_COLOR_SLIDER_CURSOR = 13
      val NK_COLOR_SLIDER_CURSOR_HOVER = 14
      val NK_COLOR_SLIDER_CURSOR_ACTIVE = 15
      val NK_COLOR_PROPERTY = 16
      val NK_COLOR_EDIT = 17
      val NK_COLOR_EDIT_CURSOR = 18
      val NK_COLOR_COMBO = 19
      val NK_COLOR_CHART = 20
      val NK_COLOR_CHART_COLOR = 21
      val NK_COLOR_CHART_COLOR_HIGHLIGHT = 22
      val NK_COLOR_SCROLLBAR = 23
      val NK_COLOR_SCROLLBAR_CURSOR = 24
      val NK_COLOR_SCROLLBAR_CURSOR_HOVER = 25
      val NK_COLOR_SCROLLBAR_CURSOR_ACTIVE = 26
      val NK_COLOR_TAB_HEADER = 27
      val NK_COLOR_COUNT = 28
    }

    object nk_style_cursor {
      val NK_CURSOR_ARROW = 0
      val NK_CURSOR_TEXT = 1
      val NK_CURSOR_MOVE = 2
      val NK_CURSOR_RESIZE_VERTICAL = 3
      val NK_CURSOR_RESIZE_HORIZONTAL = 4
      val NK_CURSOR_RESIZE_TOP_LEFT_DOWN_RIGHT = 5
      val NK_CURSOR_RESIZE_TOP_RIGHT_DOWN_LEFT = 6
      val NK_CURSOR_COUNT = 7
    }

    implicit class nk_user_fontOps(val ptr: Ptr[nk_user_font]) extends AnyVal {
      def userdata: nk_handle = !ptr._1

      def height: CFloat = !ptr._2

      def width: nk_text_width_f = !ptr._3

      def userdata_=(v: nk_handle): Unit = !ptr._1 = v

      def height_=(v: CFloat): Unit = !ptr._2 = v

      def width_=(v: nk_text_width_f): Unit = !ptr._3 = v
    }

    implicit class nk_memory_statusOps(val ptr: Ptr[nk_memory_status]) extends AnyVal {
      def memory: Ptr[Byte] = !ptr._1

      def `type`: CUnsignedInt = !ptr._2

      def size: nk_size = !ptr._3

      def allocated: nk_size = !ptr._4

      def needed: nk_size = !ptr._5

      def calls: nk_size = !ptr._6

      def memory_=(v: Ptr[Byte]): Unit = !ptr._1 = v

      def type_=(v: CUnsignedInt): Unit = !ptr._2 = v

      def size_=(v: nk_size): Unit = !ptr._3 = v

      def allocated_=(v: nk_size): Unit = !ptr._4 = v

      def needed_=(v: nk_size): Unit = !ptr._5 = v

      def calls_=(v: nk_size): Unit = !ptr._6 = v
    }

    object nk_allocation_type {
      val NK_BUFFER_FIXED = 0
      val NK_BUFFER_DYNAMIC = 1
    }

    object nk_buffer_allocation_type {
      nk_buffer_allocation_type
      nk_buffer_allocation_type
      nk_buffer_allocation_type
    }

    implicit class nk_buffer_markerOps(val ptr: Ptr[nk_buffer_marker]) extends AnyVal {
      def active: CInt = !ptr._1

      def offset: nk_size = !ptr._2

      def active_=(v: CInt): Unit = !ptr._1 = v

      def offset_=(v: nk_size): Unit = !ptr._2 = v
    }

    implicit class nk_memoryOps(val iptr: Ptr[nk_memory]) extends AnyVal {
      def ptr: Ptr[Byte] = !iptr._1

      def size: nk_size = !iptr._2

      def ptr_=(v: Ptr[Byte]): Unit = !iptr._1 = v

      def size_=(v: nk_size): Unit = !iptr._2 = v
    }

    implicit class nk_bufferOps(val ptr: Ptr[nk_buffer]) extends AnyVal {
      def marker: Ptr[nk_buffer_marker] = !ptr._1

      def pool: nk_allocator = !ptr._2

      def `type`: nk_allocation_type = !ptr._3

      def memory: nk_memory = !ptr._4

      def grow_factor: CFloat = !ptr._5

      def allocated: nk_size = !ptr._6

      def needed: nk_size = !ptr._7

      def calls: nk_size = !ptr._8

      def size: nk_size = !ptr._9

      def marker_=(v: Ptr[nk_buffer_marker]): Unit = !ptr._1 = v

      def pool_=(v: nk_allocator): Unit = !ptr._2 = v

      def type_=(v: nk_allocation_type): Unit = !ptr._3 = v

      def memory_=(v: nk_memory): Unit = !ptr._4 = v

      def grow_factor_=(v: CFloat): Unit = !ptr._5 = v

      def allocated_=(v: nk_size): Unit = !ptr._6 = v

      def needed_=(v: nk_size): Unit = !ptr._7 = v

      def calls_=(v: nk_size): Unit = !ptr._8 = v

      def size_=(v: nk_size): Unit = !ptr._9 = v
    }

    implicit class nk_strOps(val ptr: Ptr[nk_str]) extends AnyVal {
      def buffer: nk_buffer = !ptr._1

      def len: CInt = !ptr._2

      def buffer_=(v: nk_buffer): Unit = !ptr._1 = v

      def len_=(v: CInt): Unit = !ptr._2 = v
    }

    implicit class nk_clipboardOps(val ptr: Ptr[nk_clipboard]) extends AnyVal {
      def userdata: nk_handle = !ptr._1

      def paste: nk_plugin_paste = !ptr._2

      def copy: nk_plugin_copy = !ptr._3

      def userdata_=(v: nk_handle): Unit = !ptr._1 = v

      def paste_=(v: nk_plugin_paste): Unit = !ptr._2 = v

      def copy_=(v: nk_plugin_copy): Unit = !ptr._3 = v
    }

    implicit class nk_text_undo_recordOps(val ptr: Ptr[nk_text_undo_record]) extends AnyVal {
      def where: CInt = !ptr._1

      def insert_length: CShort = !ptr._2

      def delete_length: CShort = !ptr._3

      def char_storage: CShort = !ptr._4

      def where_=(v: CInt): Unit = !ptr._1 = v

      def insert_length_=(v: CShort): Unit = !ptr._2 = v

      def delete_length_=(v: CShort): Unit = !ptr._3 = v

      def char_storage_=(v: CShort): Unit = !ptr._4 = v
    }

    implicit class nk_text_undo_stateOps(val ptr: Ptr[nk_text_undo_state]) extends AnyVal {
      def undo_rec: Ptr[nk_text_undo_record] = !ptr._1

      def undo_char: Ptr[nk_rune] = !ptr._2

      def undo_point: CShort = !ptr._3

      def redo_point: CShort = !ptr._4

      def undo_char_point: CShort = !ptr._5

      def redo_char_point: CShort = !ptr._6

      def undo_rec_=(v: Ptr[nk_text_undo_record]): Unit = !ptr._1 = v

      def undo_char_=(v: Ptr[nk_rune]): Unit = !ptr._2 = v

      def undo_point_=(v: CShort): Unit = !ptr._3 = v

      def redo_point_=(v: CShort): Unit = !ptr._4 = v

      def undo_char_point_=(v: CShort): Unit = !ptr._5 = v

      def redo_char_point_=(v: CShort): Unit = !ptr._6 = v
    }

    object nk_text_edit_type {
      nk_text_edit_type
      nk_text_edit_type
    }

    object nk_text_edit_mode {
      nk_text_edit_mode
      nk_text_edit_mode
      nk_text_edit_mode
    }

    implicit class nk_text_editOps(val ptr: Ptr[nk_text_edit]) extends AnyVal {
      def clip: nk_clipboard = !ptr._1

      def string: nk_str = !ptr._2

      def filter: nk_plugin_filter = !ptr._3

      def scrollbar: nk_vec2 = !ptr._4

      def cursor: CInt = !ptr._5

      def select_start: CInt = !ptr._6

      def select_end: CInt = !ptr._7

      def mode: CUnsignedChar = !ptr._8

      def cursor_at_end_of_line: CUnsignedChar = !ptr._9

      def initialized: CUnsignedChar = !ptr._10

      def has_preferred_x: CUnsignedChar = !ptr._11

      def single_line: CUnsignedChar = !ptr._12

      def active: CUnsignedChar = !ptr._13

      def padding1: CUnsignedChar = !ptr._14

      def preferred_x: CFloat = !ptr._15

      def undo: nk_text_undo_state = !ptr._16

      def clip_=(v: nk_clipboard): Unit = !ptr._1 = v

      def string_=(v: nk_str): Unit = !ptr._2 = v

      def filter_=(v: nk_plugin_filter): Unit = !ptr._3 = v

      def scrollbar_=(v: nk_vec2): Unit = !ptr._4 = v

      def cursor_=(v: CInt): Unit = !ptr._5 = v

      def select_start_=(v: CInt): Unit = !ptr._6 = v

      def select_end_=(v: CInt): Unit = !ptr._7 = v

      def mode_=(v: CUnsignedChar): Unit = !ptr._8 = v

      def cursor_at_end_of_line_=(v: CUnsignedChar): Unit = !ptr._9 = v

      def initialized_=(v: CUnsignedChar): Unit = !ptr._10 = v

      def has_preferred_x_=(v: CUnsignedChar): Unit = !ptr._11 = v

      def single_line_=(v: CUnsignedChar): Unit = !ptr._12 = v

      def active_=(v: CUnsignedChar): Unit = !ptr._13 = v

      def padding1_=(v: CUnsignedChar): Unit = !ptr._14 = v

      def preferred_x_=(v: CFloat): Unit = !ptr._15 = v

      def undo_=(v: nk_text_undo_state): Unit = !ptr._16 = v
    }

    object nk_command_type {
      val NK_COMMAND_NOP = 0
      val NK_COMMAND_SCISSOR = 1
      val NK_COMMAND_LINE = 2
      val NK_COMMAND_CURVE = 3
      val NK_COMMAND_RECT = 4
      val NK_COMMAND_RECT_FILLED = 5
      val NK_COMMAND_RECT_MULTI_COLOR = 6
      val NK_COMMAND_CIRCLE = 7
      val NK_COMMAND_CIRCLE_FILLED = 8
      val NK_COMMAND_ARC = 9
      val NK_COMMAND_ARC_FILLED = 10
      val NK_COMMAND_TRIANGLE = 11
      val NK_COMMAND_TRIANGLE_FILLED = 12
      val NK_COMMAND_POLYGON = 13
      val NK_COMMAND_POLYGON_FILLED = 14
      val NK_COMMAND_POLYLINE = 15
      val NK_COMMAND_TEXT = 16
      val NK_COMMAND_IMAGE = 17
      val NK_COMMAND_CUSTOM = 18
    }

    implicit class nk_commandOps(val ptr: Ptr[nk_command]) extends AnyVal {
      def `type`: nk_command_type = !ptr._1

      def next: nk_size = !ptr._2

      def type_=(v: nk_command_type): Unit = !ptr._1 = v

      def next_=(v: nk_size): Unit = !ptr._2 = v
    }

    implicit class nk_command_scissorOps(val ptr: Ptr[nk_command_scissor]) extends AnyVal {
      def header: nk_command = !ptr._1

      def x: CShort = !ptr._2

      def y: CShort = !ptr._3

      def w: CUnsignedShort = !ptr._4

      def h: CUnsignedShort = !ptr._5

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def x_=(v: CShort): Unit = !ptr._2 = v

      def y_=(v: CShort): Unit = !ptr._3 = v

      def w_=(v: CUnsignedShort): Unit = !ptr._4 = v

      def h_=(v: CUnsignedShort): Unit = !ptr._5 = v
    }

    implicit class nk_command_lineOps(val ptr: Ptr[nk_command_line]) extends AnyVal {
      def header: nk_command = !ptr._1

      def line_thickness: CUnsignedShort = !ptr._2

      def begin: nk_vec2i = !ptr._3

      def end: nk_vec2i = !ptr._4

      def color: nk_color = !ptr._5

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def line_thickness_=(v: CUnsignedShort): Unit = !ptr._2 = v

      def begin_=(v: nk_vec2i): Unit = !ptr._3 = v

      def end_=(v: nk_vec2i): Unit = !ptr._4 = v

      def color_=(v: nk_color): Unit = !ptr._5 = v
    }

    implicit class nk_command_curveOps(val ptr: Ptr[nk_command_curve]) extends AnyVal {
      def header: nk_command = !ptr._1

      def line_thickness: CUnsignedShort = !ptr._2

      def begin: nk_vec2i = !ptr._3

      def end: nk_vec2i = !ptr._4

      def ctrl: Ptr[nk_vec2i] = !ptr._5

      def color: nk_color = !ptr._6

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def line_thickness_=(v: CUnsignedShort): Unit = !ptr._2 = v

      def begin_=(v: nk_vec2i): Unit = !ptr._3 = v

      def end_=(v: nk_vec2i): Unit = !ptr._4 = v

      def ctrl_=(v: Ptr[nk_vec2i]): Unit = !ptr._5 = v

      def color_=(v: nk_color): Unit = !ptr._6 = v
    }

    implicit class nk_command_rectOps(val ptr: Ptr[nk_command_rect]) extends AnyVal {
      def header: nk_command = !ptr._1

      def rounding: CUnsignedShort = !ptr._2

      def line_thickness: CUnsignedShort = !ptr._3

      def x: CShort = !ptr._4

      def y: CShort = !ptr._5

      def w: CUnsignedShort = !ptr._6

      def h: CUnsignedShort = !ptr._7

      def color: nk_color = !ptr._8

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def rounding_=(v: CUnsignedShort): Unit = !ptr._2 = v

      def line_thickness_=(v: CUnsignedShort): Unit = !ptr._3 = v

      def x_=(v: CShort): Unit = !ptr._4 = v

      def y_=(v: CShort): Unit = !ptr._5 = v

      def w_=(v: CUnsignedShort): Unit = !ptr._6 = v

      def h_=(v: CUnsignedShort): Unit = !ptr._7 = v

      def color_=(v: nk_color): Unit = !ptr._8 = v
    }

    implicit class nk_command_rect_filledOps(val ptr: Ptr[nk_command_rect_filled]) extends AnyVal {
      def header: nk_command = !ptr._1

      def rounding: CUnsignedShort = !ptr._2

      def x: CShort = !ptr._3

      def y: CShort = !ptr._4

      def w: CUnsignedShort = !ptr._5

      def h: CUnsignedShort = !ptr._6

      def color: nk_color = !ptr._7

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def rounding_=(v: CUnsignedShort): Unit = !ptr._2 = v

      def x_=(v: CShort): Unit = !ptr._3 = v

      def y_=(v: CShort): Unit = !ptr._4 = v

      def w_=(v: CUnsignedShort): Unit = !ptr._5 = v

      def h_=(v: CUnsignedShort): Unit = !ptr._6 = v

      def color_=(v: nk_color): Unit = !ptr._7 = v
    }

    implicit class nk_command_rect_multi_colorOps(val ptr: Ptr[nk_command_rect_multi_color]) extends AnyVal {
      def header: nk_command = !ptr._1

      def x: CShort = !ptr._2

      def y: CShort = !ptr._3

      def w: CUnsignedShort = !ptr._4

      def h: CUnsignedShort = !ptr._5

      def left: nk_color = !ptr._6

      def top: nk_color = !ptr._7

      def bottom: nk_color = !ptr._8

      def right: nk_color = !ptr._9

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def x_=(v: CShort): Unit = !ptr._2 = v

      def y_=(v: CShort): Unit = !ptr._3 = v

      def w_=(v: CUnsignedShort): Unit = !ptr._4 = v

      def h_=(v: CUnsignedShort): Unit = !ptr._5 = v

      def left_=(v: nk_color): Unit = !ptr._6 = v

      def top_=(v: nk_color): Unit = !ptr._7 = v

      def bottom_=(v: nk_color): Unit = !ptr._8 = v

      def right_=(v: nk_color): Unit = !ptr._9 = v
    }

    implicit class nk_command_triangleOps(val ptr: Ptr[nk_command_triangle]) extends AnyVal {
      def header: nk_command = !ptr._1

      def line_thickness: CUnsignedShort = !ptr._2

      def a: nk_vec2i = !ptr._3

      def b: nk_vec2i = !ptr._4

      def c: nk_vec2i = !ptr._5

      def color: nk_color = !ptr._6

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def line_thickness_=(v: CUnsignedShort): Unit = !ptr._2 = v

      def a_=(v: nk_vec2i): Unit = !ptr._3 = v

      def b_=(v: nk_vec2i): Unit = !ptr._4 = v

      def c_=(v: nk_vec2i): Unit = !ptr._5 = v

      def color_=(v: nk_color): Unit = !ptr._6 = v
    }

    implicit class nk_command_triangle_filledOps(val ptr: Ptr[nk_command_triangle_filled]) extends AnyVal {
      def header: nk_command = !ptr._1

      def a: nk_vec2i = !ptr._2

      def b: nk_vec2i = !ptr._3

      def c: nk_vec2i = !ptr._4

      def color: nk_color = !ptr._5

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def a_=(v: nk_vec2i): Unit = !ptr._2 = v

      def b_=(v: nk_vec2i): Unit = !ptr._3 = v

      def c_=(v: nk_vec2i): Unit = !ptr._4 = v

      def color_=(v: nk_color): Unit = !ptr._5 = v
    }

    implicit class nk_command_circleOps(val ptr: Ptr[nk_command_circle]) extends AnyVal {
      def header: nk_command = !ptr._1

      def x: CShort = !ptr._2

      def y: CShort = !ptr._3

      def line_thickness: CUnsignedShort = !ptr._4

      def w: CUnsignedShort = !ptr._5

      def h: CUnsignedShort = !ptr._6

      def color: nk_color = !ptr._7

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def x_=(v: CShort): Unit = !ptr._2 = v

      def y_=(v: CShort): Unit = !ptr._3 = v

      def line_thickness_=(v: CUnsignedShort): Unit = !ptr._4 = v

      def w_=(v: CUnsignedShort): Unit = !ptr._5 = v

      def h_=(v: CUnsignedShort): Unit = !ptr._6 = v

      def color_=(v: nk_color): Unit = !ptr._7 = v
    }

    implicit class nk_command_circle_filledOps(val ptr: Ptr[nk_command_circle_filled]) extends AnyVal {
      def header: nk_command = !ptr._1

      def x: CShort = !ptr._2

      def y: CShort = !ptr._3

      def w: CUnsignedShort = !ptr._4

      def h: CUnsignedShort = !ptr._5

      def color: nk_color = !ptr._6

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def x_=(v: CShort): Unit = !ptr._2 = v

      def y_=(v: CShort): Unit = !ptr._3 = v

      def w_=(v: CUnsignedShort): Unit = !ptr._4 = v

      def h_=(v: CUnsignedShort): Unit = !ptr._5 = v

      def color_=(v: nk_color): Unit = !ptr._6 = v
    }

    implicit class nk_command_arcOps(val ptr: Ptr[nk_command_arc]) extends AnyVal {
      def header: nk_command = !ptr._1

      def cx: CShort = !ptr._2

      def cy: CShort = !ptr._3

      def r: CUnsignedShort = !ptr._4

      def line_thickness: CUnsignedShort = !ptr._5

      def a: Ptr[CFloat] = !ptr._6

      def color: nk_color = !ptr._7

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def cx_=(v: CShort): Unit = !ptr._2 = v

      def cy_=(v: CShort): Unit = !ptr._3 = v

      def r_=(v: CUnsignedShort): Unit = !ptr._4 = v

      def line_thickness_=(v: CUnsignedShort): Unit = !ptr._5 = v

      def a_=(v: Ptr[CFloat]): Unit = !ptr._6 = v

      def color_=(v: nk_color): Unit = !ptr._7 = v
    }

    implicit class nk_command_arc_filledOps(val ptr: Ptr[nk_command_arc_filled]) extends AnyVal {
      def header: nk_command = !ptr._1

      def cx: CShort = !ptr._2

      def cy: CShort = !ptr._3

      def r: CUnsignedShort = !ptr._4

      def a: Ptr[CFloat] = !ptr._5

      def color: nk_color = !ptr._6

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def cx_=(v: CShort): Unit = !ptr._2 = v

      def cy_=(v: CShort): Unit = !ptr._3 = v

      def r_=(v: CUnsignedShort): Unit = !ptr._4 = v

      def a_=(v: Ptr[CFloat]): Unit = !ptr._5 = v

      def color_=(v: nk_color): Unit = !ptr._6 = v
    }

    implicit class nk_command_polygonOps(val ptr: Ptr[nk_command_polygon]) extends AnyVal {
      def header: nk_command = !ptr._1

      def color: nk_color = !ptr._2

      def line_thickness: CUnsignedShort = !ptr._3

      def point_count: CUnsignedShort = !ptr._4

      def points: Ptr[nk_vec2i] = !ptr._5

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def color_=(v: nk_color): Unit = !ptr._2 = v

      def line_thickness_=(v: CUnsignedShort): Unit = !ptr._3 = v

      def point_count_=(v: CUnsignedShort): Unit = !ptr._4 = v

      def points_=(v: Ptr[nk_vec2i]): Unit = !ptr._5 = v
    }

    implicit class nk_command_polygon_filledOps(val ptr: Ptr[nk_command_polygon_filled]) extends AnyVal {
      def header: nk_command = !ptr._1

      def color: nk_color = !ptr._2

      def point_count: CUnsignedShort = !ptr._3

      def points: Ptr[nk_vec2i] = !ptr._4

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def color_=(v: nk_color): Unit = !ptr._2 = v

      def point_count_=(v: CUnsignedShort): Unit = !ptr._3 = v

      def points_=(v: Ptr[nk_vec2i]): Unit = !ptr._4 = v
    }

    implicit class nk_command_polylineOps(val ptr: Ptr[nk_command_polyline]) extends AnyVal {
      def header: nk_command = !ptr._1

      def color: nk_color = !ptr._2

      def line_thickness: CUnsignedShort = !ptr._3

      def point_count: CUnsignedShort = !ptr._4

      def points: Ptr[nk_vec2i] = !ptr._5

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def color_=(v: nk_color): Unit = !ptr._2 = v

      def line_thickness_=(v: CUnsignedShort): Unit = !ptr._3 = v

      def point_count_=(v: CUnsignedShort): Unit = !ptr._4 = v

      def points_=(v: Ptr[nk_vec2i]): Unit = !ptr._5 = v
    }

    implicit class nk_command_imageOps(val ptr: Ptr[nk_command_image]) extends AnyVal {
      def header: nk_command = !ptr._1

      def x: CShort = !ptr._2

      def y: CShort = !ptr._3

      def w: CUnsignedShort = !ptr._4

      def h: CUnsignedShort = !ptr._5

      def img: nk_image = !ptr._6

      def col: nk_color = !ptr._7

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def x_=(v: CShort): Unit = !ptr._2 = v

      def y_=(v: CShort): Unit = !ptr._3 = v

      def w_=(v: CUnsignedShort): Unit = !ptr._4 = v

      def h_=(v: CUnsignedShort): Unit = !ptr._5 = v

      def img_=(v: nk_image): Unit = !ptr._6 = v

      def col_=(v: nk_color): Unit = !ptr._7 = v
    }

    implicit class nk_command_customOps(val ptr: Ptr[nk_command_custom]) extends AnyVal {
      def header: nk_command = !ptr._1

      def x: CShort = !ptr._2

      def y: CShort = !ptr._3

      def w: CUnsignedShort = !ptr._4

      def h: CUnsignedShort = !ptr._5

      def callback_data: nk_handle = !ptr._6

      def callback: nk_command_custom_callback = !ptr._7

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def x_=(v: CShort): Unit = !ptr._2 = v

      def y_=(v: CShort): Unit = !ptr._3 = v

      def w_=(v: CUnsignedShort): Unit = !ptr._4 = v

      def h_=(v: CUnsignedShort): Unit = !ptr._5 = v

      def callback_data_=(v: nk_handle): Unit = !ptr._6 = v

      def callback_=(v: nk_command_custom_callback): Unit = !ptr._7 = v
    }

    implicit class nk_command_textOps(val ptr: Ptr[nk_command_text]) extends AnyVal {
      def header: nk_command = !ptr._1

      def font: Ptr[nk_user_font] = !ptr._2

      def background: nk_color = !ptr._3

      def foreground: nk_color = !ptr._4

      def x: CShort = !ptr._5

      def y: CShort = !ptr._6

      def w: CUnsignedShort = !ptr._7

      def h: CUnsignedShort = !ptr._8

      def height: CFloat = !ptr._9

      def length: CInt = !ptr._10

      def string: CString = !ptr._11

      def header_=(v: nk_command): Unit = !ptr._1 = v

      def font_=(v: Ptr[nk_user_font]): Unit = !ptr._2 = v

      def background_=(v: nk_color): Unit = !ptr._3 = v

      def foreground_=(v: nk_color): Unit = !ptr._4 = v

      def x_=(v: CShort): Unit = !ptr._5 = v

      def y_=(v: CShort): Unit = !ptr._6 = v

      def w_=(v: CUnsignedShort): Unit = !ptr._7 = v

      def h_=(v: CUnsignedShort): Unit = !ptr._8 = v

      def height_=(v: CFloat): Unit = !ptr._9 = v

      def length_=(v: CInt): Unit = !ptr._10 = v

      def string_=(v: CString): Unit = !ptr._11 = v
    }

    object nk_command_clipping {
      nk_command_clipping
      nk_command_clipping
    }

    implicit class nk_command_bufferOps(val ptr: Ptr[nk_command_buffer]) extends AnyVal {
      def base: Ptr[nk_buffer] = !ptr._1

      def clip: nk_rect = !ptr._2

      def use_clipping: CInt = !ptr._3

      def userdata: nk_handle = !ptr._4

      def begin: nk_size = !ptr._5

      def end: nk_size = !ptr._6

      def last: nk_size = !ptr._7

      def base_=(v: Ptr[nk_buffer]): Unit = !ptr._1 = v

      def clip_=(v: nk_rect): Unit = !ptr._2 = v

      def use_clipping_=(v: CInt): Unit = !ptr._3 = v

      def userdata_=(v: nk_handle): Unit = !ptr._4 = v

      def begin_=(v: nk_size): Unit = !ptr._5 = v

      def end_=(v: nk_size): Unit = !ptr._6 = v

      def last_=(v: nk_size): Unit = !ptr._7 = v
    }

    implicit class nk_mouse_buttonOps(val ptr: Ptr[nk_mouse_button]) extends AnyVal {
      def down: CInt = !ptr._1

      def clicked: CUnsignedInt = !ptr._2

      def clicked_pos: nk_vec2 = !ptr._3

      def down_=(v: CInt): Unit = !ptr._1 = v

      def clicked_=(v: CUnsignedInt): Unit = !ptr._2 = v

      def clicked_pos_=(v: nk_vec2): Unit = !ptr._3 = v
    }

    implicit class nk_mouseOps(val ptr: Ptr[nk_mouse]) extends AnyVal {
      def buttons: Ptr[nk_mouse_button] = !ptr._1

      def pos: nk_vec2 = !ptr._2

      def prev: nk_vec2 = !ptr._3

      def delta: nk_vec2 = !ptr._4

      def scroll_delta: nk_vec2 = !ptr._5

      def grab: CUnsignedChar = !ptr._6

      def grabbed: CUnsignedChar = !ptr._7

      def ungrab: CUnsignedChar = !ptr._8

      def buttons_=(v: Ptr[nk_mouse_button]): Unit = !ptr._1 = v

      def pos_=(v: nk_vec2): Unit = !ptr._2 = v

      def prev_=(v: nk_vec2): Unit = !ptr._3 = v

      def delta_=(v: nk_vec2): Unit = !ptr._4 = v

      def scroll_delta_=(v: nk_vec2): Unit = !ptr._5 = v

      def grab_=(v: CUnsignedChar): Unit = !ptr._6 = v

      def grabbed_=(v: CUnsignedChar): Unit = !ptr._7 = v

      def ungrab_=(v: CUnsignedChar): Unit = !ptr._8 = v
    }

    implicit class nk_keyOps(val ptr: Ptr[nk_key]) extends AnyVal {
      def down: CInt = !ptr._1

      def clicked: CUnsignedInt = !ptr._2

      def down_=(v: CInt): Unit = !ptr._1 = v

      def clicked_=(v: CUnsignedInt): Unit = !ptr._2 = v
    }

    implicit class nk_keyboardOps(val ptr: Ptr[nk_keyboard]) extends AnyVal {
      def keys: Ptr[nk_key] = !ptr._1

      def text: CString = !ptr._2

      def text_len: CInt = !ptr._3

      def keys_=(v: Ptr[nk_key]): Unit = !ptr._1 = v

      def text_=(v: CString): Unit = !ptr._2 = v

      def text_len_=(v: CInt): Unit = !ptr._3 = v
    }

    implicit class nk_inputOps(val ptr: Ptr[nk_input]) extends AnyVal {
      def keyboard: nk_keyboard = !ptr._1

      def mouse: nk_mouse = !ptr._2

      def keyboard_=(v: nk_keyboard): Unit = !ptr._1 = v

      def mouse_=(v: nk_mouse): Unit = !ptr._2 = v
    }

    object nk_style_item_type {
      nk_style_item_type
      nk_style_item_type
    }

    implicit class nk_style_textOps(val ptr: Ptr[nk_style_text]) extends AnyVal {
      def color: nk_color = !ptr._1

      def padding: nk_vec2 = !ptr._2

      def color_=(v: nk_color): Unit = !ptr._1 = v

      def padding_=(v: nk_vec2): Unit = !ptr._2 = v
    }

    implicit class nk_style_buttonOps(val ptr: Ptr[nk_style_button]) extends AnyVal {
      def normal: nk_style_item = !ptr._1

      def hover: nk_style_item = !ptr._2

      def active: nk_style_item = !ptr._3

      def border_color: nk_color = !ptr._4

      def text_background: nk_color = !ptr._5

      def text_normal: nk_color = !ptr._6

      def text_hover: nk_color = !ptr._7

      def text_active: nk_color = !ptr._8

      def text_alignment: nk_flags = !ptr._9

      def border: CFloat = !ptr._10

      def rounding: CFloat = !ptr._11

      def padding: nk_vec2 = !ptr._12

      def image_padding: nk_vec2 = !ptr._13

      def touch_padding: nk_vec2 = !ptr._14

      def userdata: nk_handle = !ptr._15

      def draw_begin: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit] = !ptr._16

      def draw_end: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit] = !ptr._17

      def normal_=(v: nk_style_item): Unit = !ptr._1 = v

      def hover_=(v: nk_style_item): Unit = !ptr._2 = v

      def active_=(v: nk_style_item): Unit = !ptr._3 = v

      def border_color_=(v: nk_color): Unit = !ptr._4 = v

      def text_background_=(v: nk_color): Unit = !ptr._5 = v

      def text_normal_=(v: nk_color): Unit = !ptr._6 = v

      def text_hover_=(v: nk_color): Unit = !ptr._7 = v

      def text_active_=(v: nk_color): Unit = !ptr._8 = v

      def text_alignment_=(v: nk_flags): Unit = !ptr._9 = v

      def border_=(v: CFloat): Unit = !ptr._10 = v

      def rounding_=(v: CFloat): Unit = !ptr._11 = v

      def padding_=(v: nk_vec2): Unit = !ptr._12 = v

      def image_padding_=(v: nk_vec2): Unit = !ptr._13 = v

      def touch_padding_=(v: nk_vec2): Unit = !ptr._14 = v

      def userdata_=(v: nk_handle): Unit = !ptr._15 = v

      def draw_begin_=(v: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]): Unit = !ptr._16 = v

      def draw_end_=(v: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]): Unit = !ptr._17 = v
    }

    implicit class nk_style_toggleOps(val ptr: Ptr[nk_style_toggle]) extends AnyVal {
      def normal: nk_style_item = !ptr._1

      def hover: nk_style_item = !ptr._2

      def active: nk_style_item = !ptr._3

      def border_color: nk_color = !ptr._4

      def cursor_normal: nk_style_item = !ptr._5

      def cursor_hover: nk_style_item = !ptr._6

      def text_normal: nk_color = !ptr._7

      def text_hover: nk_color = !ptr._8

      def text_active: nk_color = !ptr._9

      def text_background: nk_color = !ptr._10

      def text_alignment: nk_flags = !ptr._11

      def padding: nk_vec2 = !ptr._12

      def touch_padding: nk_vec2 = !ptr._13

      def spacing: CFloat = !ptr._14

      def border: CFloat = !ptr._15

      def userdata: nk_handle = !ptr._16

      def draw_begin: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit] = !ptr._17

      def draw_end: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit] = !ptr._18

      def normal_=(v: nk_style_item): Unit = !ptr._1 = v

      def hover_=(v: nk_style_item): Unit = !ptr._2 = v

      def active_=(v: nk_style_item): Unit = !ptr._3 = v

      def border_color_=(v: nk_color): Unit = !ptr._4 = v

      def cursor_normal_=(v: nk_style_item): Unit = !ptr._5 = v

      def cursor_hover_=(v: nk_style_item): Unit = !ptr._6 = v

      def text_normal_=(v: nk_color): Unit = !ptr._7 = v

      def text_hover_=(v: nk_color): Unit = !ptr._8 = v

      def text_active_=(v: nk_color): Unit = !ptr._9 = v

      def text_background_=(v: nk_color): Unit = !ptr._10 = v

      def text_alignment_=(v: nk_flags): Unit = !ptr._11 = v

      def padding_=(v: nk_vec2): Unit = !ptr._12 = v

      def touch_padding_=(v: nk_vec2): Unit = !ptr._13 = v

      def spacing_=(v: CFloat): Unit = !ptr._14 = v

      def border_=(v: CFloat): Unit = !ptr._15 = v

      def userdata_=(v: nk_handle): Unit = !ptr._16 = v

      def draw_begin_=(v: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]): Unit = !ptr._17 = v

      def draw_end_=(v: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]): Unit = !ptr._18 = v
    }

    implicit class nk_style_selectableOps(val ptr: Ptr[nk_style_selectable]) extends AnyVal {
      def normal: nk_style_item = !ptr._1

      def hover: nk_style_item = !ptr._2

      def pressed: nk_style_item = !ptr._3

      def normal_active: nk_style_item = !ptr._4

      def hover_active: nk_style_item = !ptr._5

      def pressed_active: nk_style_item = !ptr._6

      def text_normal: nk_color = !ptr._7

      def text_hover: nk_color = !ptr._8

      def text_pressed: nk_color = !ptr._9

      def text_normal_active: nk_color = !ptr._10

      def text_hover_active: nk_color = !ptr._11

      def text_pressed_active: nk_color = !ptr._12

      def text_background: nk_color = !ptr._13

      def text_alignment: nk_flags = !ptr._14

      def rounding: CFloat = !ptr._15

      def padding: nk_vec2 = !ptr._16

      def touch_padding: nk_vec2 = !ptr._17

      def image_padding: nk_vec2 = !ptr._18

      def userdata: nk_handle = !ptr._19

      def draw_begin: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit] = !ptr._20

      def draw_end: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit] = !ptr._21

      def normal_=(v: nk_style_item): Unit = !ptr._1 = v

      def hover_=(v: nk_style_item): Unit = !ptr._2 = v

      def pressed_=(v: nk_style_item): Unit = !ptr._3 = v

      def normal_active_=(v: nk_style_item): Unit = !ptr._4 = v

      def hover_active_=(v: nk_style_item): Unit = !ptr._5 = v

      def pressed_active_=(v: nk_style_item): Unit = !ptr._6 = v

      def text_normal_=(v: nk_color): Unit = !ptr._7 = v

      def text_hover_=(v: nk_color): Unit = !ptr._8 = v

      def text_pressed_=(v: nk_color): Unit = !ptr._9 = v

      def text_normal_active_=(v: nk_color): Unit = !ptr._10 = v

      def text_hover_active_=(v: nk_color): Unit = !ptr._11 = v

      def text_pressed_active_=(v: nk_color): Unit = !ptr._12 = v

      def text_background_=(v: nk_color): Unit = !ptr._13 = v

      def text_alignment_=(v: nk_flags): Unit = !ptr._14 = v

      def rounding_=(v: CFloat): Unit = !ptr._15 = v

      def padding_=(v: nk_vec2): Unit = !ptr._16 = v

      def touch_padding_=(v: nk_vec2): Unit = !ptr._17 = v

      def image_padding_=(v: nk_vec2): Unit = !ptr._18 = v

      def userdata_=(v: nk_handle): Unit = !ptr._19 = v

      def draw_begin_=(v: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]): Unit = !ptr._20 = v

      def draw_end_=(v: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]): Unit = !ptr._21 = v
    }

    implicit class nk_style_sliderOps(val ptr: Ptr[nk_style_slider]) extends AnyVal {
      def normal: nk_style_item = !ptr._1

      def hover: nk_style_item = !ptr._2

      def active: nk_style_item = !ptr._3

      def border_color: nk_color = !ptr._4

      def bar_normal: nk_color = !ptr._5

      def bar_hover: nk_color = !ptr._6

      def bar_active: nk_color = !ptr._7

      def bar_filled: nk_color = !ptr._8

      def cursor_normal: nk_style_item = !ptr._9

      def cursor_hover: nk_style_item = !ptr._10

      def cursor_active: nk_style_item = !ptr._11

      def border: CFloat = !ptr._12

      def rounding: CFloat = !ptr._13

      def bar_height: CFloat = !ptr._14

      def padding: nk_vec2 = !ptr._15

      def spacing: nk_vec2 = !ptr._16

      def cursor_size: nk_vec2 = !ptr._17

      def show_buttons: CInt = !ptr._18

      def inc_button: nk_style_button = !ptr._19

      def dec_button: nk_style_button = !ptr._20

      def inc_symbol: nk_symbol_type = !ptr._21

      def dec_symbol: nk_symbol_type = !ptr._22

      def userdata: nk_handle = !ptr._23

      def draw_begin: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit] = !ptr._24

      def draw_end: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit] = !ptr._25

      def normal_=(v: nk_style_item): Unit = !ptr._1 = v

      def hover_=(v: nk_style_item): Unit = !ptr._2 = v

      def active_=(v: nk_style_item): Unit = !ptr._3 = v

      def border_color_=(v: nk_color): Unit = !ptr._4 = v

      def bar_normal_=(v: nk_color): Unit = !ptr._5 = v

      def bar_hover_=(v: nk_color): Unit = !ptr._6 = v

      def bar_active_=(v: nk_color): Unit = !ptr._7 = v

      def bar_filled_=(v: nk_color): Unit = !ptr._8 = v

      def cursor_normal_=(v: nk_style_item): Unit = !ptr._9 = v

      def cursor_hover_=(v: nk_style_item): Unit = !ptr._10 = v

      def cursor_active_=(v: nk_style_item): Unit = !ptr._11 = v

      def border_=(v: CFloat): Unit = !ptr._12 = v

      def rounding_=(v: CFloat): Unit = !ptr._13 = v

      def bar_height_=(v: CFloat): Unit = !ptr._14 = v

      def padding_=(v: nk_vec2): Unit = !ptr._15 = v

      def spacing_=(v: nk_vec2): Unit = !ptr._16 = v

      def cursor_size_=(v: nk_vec2): Unit = !ptr._17 = v

      def show_buttons_=(v: CInt): Unit = !ptr._18 = v

      def inc_button_=(v: nk_style_button): Unit = !ptr._19 = v

      def dec_button_=(v: nk_style_button): Unit = !ptr._20 = v

      def inc_symbol_=(v: nk_symbol_type): Unit = !ptr._21 = v

      def dec_symbol_=(v: nk_symbol_type): Unit = !ptr._22 = v

      def userdata_=(v: nk_handle): Unit = !ptr._23 = v

      def draw_begin_=(v: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]): Unit = !ptr._24 = v

      def draw_end_=(v: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]): Unit = !ptr._25 = v
    }

    implicit class nk_style_progressOps(val ptr: Ptr[nk_style_progress]) extends AnyVal {
      def normal: nk_style_item = !ptr._1

      def hover: nk_style_item = !ptr._2

      def active: nk_style_item = !ptr._3

      def border_color: nk_color = !ptr._4

      def cursor_normal: nk_style_item = !ptr._5

      def cursor_hover: nk_style_item = !ptr._6

      def cursor_active: nk_style_item = !ptr._7

      def cursor_border_color: nk_color = !ptr._8

      def rounding: CFloat = !ptr._9

      def border: CFloat = !ptr._10

      def cursor_border: CFloat = !ptr._11

      def cursor_rounding: CFloat = !ptr._12

      def padding: nk_vec2 = !ptr._13

      def userdata: nk_handle = !ptr._14

      def draw_begin: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit] = !ptr._15

      def draw_end: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit] = !ptr._16

      def normal_=(v: nk_style_item): Unit = !ptr._1 = v

      def hover_=(v: nk_style_item): Unit = !ptr._2 = v

      def active_=(v: nk_style_item): Unit = !ptr._3 = v

      def border_color_=(v: nk_color): Unit = !ptr._4 = v

      def cursor_normal_=(v: nk_style_item): Unit = !ptr._5 = v

      def cursor_hover_=(v: nk_style_item): Unit = !ptr._6 = v

      def cursor_active_=(v: nk_style_item): Unit = !ptr._7 = v

      def cursor_border_color_=(v: nk_color): Unit = !ptr._8 = v

      def rounding_=(v: CFloat): Unit = !ptr._9 = v

      def border_=(v: CFloat): Unit = !ptr._10 = v

      def cursor_border_=(v: CFloat): Unit = !ptr._11 = v

      def cursor_rounding_=(v: CFloat): Unit = !ptr._12 = v

      def padding_=(v: nk_vec2): Unit = !ptr._13 = v

      def userdata_=(v: nk_handle): Unit = !ptr._14 = v

      def draw_begin_=(v: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]): Unit = !ptr._15 = v

      def draw_end_=(v: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]): Unit = !ptr._16 = v
    }

    implicit class nk_style_scrollbarOps(val ptr: Ptr[nk_style_scrollbar]) extends AnyVal {
      def normal: nk_style_item = !ptr._1

      def hover: nk_style_item = !ptr._2

      def active: nk_style_item = !ptr._3

      def border_color: nk_color = !ptr._4

      def cursor_normal: nk_style_item = !ptr._5

      def cursor_hover: nk_style_item = !ptr._6

      def cursor_active: nk_style_item = !ptr._7

      def cursor_border_color: nk_color = !ptr._8

      def border: CFloat = !ptr._9

      def rounding: CFloat = !ptr._10

      def border_cursor: CFloat = !ptr._11

      def rounding_cursor: CFloat = !ptr._12

      def padding: nk_vec2 = !ptr._13

      def show_buttons: CInt = !ptr._14

      def inc_button: nk_style_button = !ptr._15

      def dec_button: nk_style_button = !ptr._16

      def inc_symbol: nk_symbol_type = !ptr._17

      def dec_symbol: nk_symbol_type = !ptr._18

      def userdata: nk_handle = !ptr._19

      def draw_begin: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit] = !ptr._20

      def draw_end: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit] = !ptr._21

      def normal_=(v: nk_style_item): Unit = !ptr._1 = v

      def hover_=(v: nk_style_item): Unit = !ptr._2 = v

      def active_=(v: nk_style_item): Unit = !ptr._3 = v

      def border_color_=(v: nk_color): Unit = !ptr._4 = v

      def cursor_normal_=(v: nk_style_item): Unit = !ptr._5 = v

      def cursor_hover_=(v: nk_style_item): Unit = !ptr._6 = v

      def cursor_active_=(v: nk_style_item): Unit = !ptr._7 = v

      def cursor_border_color_=(v: nk_color): Unit = !ptr._8 = v

      def border_=(v: CFloat): Unit = !ptr._9 = v

      def rounding_=(v: CFloat): Unit = !ptr._10 = v

      def border_cursor_=(v: CFloat): Unit = !ptr._11 = v

      def rounding_cursor_=(v: CFloat): Unit = !ptr._12 = v

      def padding_=(v: nk_vec2): Unit = !ptr._13 = v

      def show_buttons_=(v: CInt): Unit = !ptr._14 = v

      def inc_button_=(v: nk_style_button): Unit = !ptr._15 = v

      def dec_button_=(v: nk_style_button): Unit = !ptr._16 = v

      def inc_symbol_=(v: nk_symbol_type): Unit = !ptr._17 = v

      def dec_symbol_=(v: nk_symbol_type): Unit = !ptr._18 = v

      def userdata_=(v: nk_handle): Unit = !ptr._19 = v

      def draw_begin_=(v: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]): Unit = !ptr._20 = v

      def draw_end_=(v: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]): Unit = !ptr._21 = v
    }

    implicit class nk_style_editOps(val ptr: Ptr[nk_style_edit]) extends AnyVal {
      def normal: nk_style_item = !ptr._1

      def hover: nk_style_item = !ptr._2

      def active: nk_style_item = !ptr._3

      def border_color: nk_color = !ptr._4

      def scrollbar: nk_style_scrollbar = !ptr._5

      def cursor_normal: nk_color = !ptr._6

      def cursor_hover: nk_color = !ptr._7

      def cursor_text_normal: nk_color = !ptr._8

      def cursor_text_hover: nk_color = !ptr._9

      def text_normal: nk_color = !ptr._10

      def text_hover: nk_color = !ptr._11

      def text_active: nk_color = !ptr._12

      def selected_normal: nk_color = !ptr._13

      def selected_hover: nk_color = !ptr._14

      def selected_text_normal: nk_color = !ptr._15

      def selected_text_hover: nk_color = !ptr._16

      def border: CFloat = !ptr._17

      def rounding: CFloat = !ptr._18

      def cursor_size: CFloat = !ptr._19

      def scrollbar_size: nk_vec2 = !ptr._20

      def padding: nk_vec2 = !ptr._21

      def row_padding: CFloat = !ptr._22

      def normal_=(v: nk_style_item): Unit = !ptr._1 = v

      def hover_=(v: nk_style_item): Unit = !ptr._2 = v

      def active_=(v: nk_style_item): Unit = !ptr._3 = v

      def border_color_=(v: nk_color): Unit = !ptr._4 = v

      def scrollbar_=(v: nk_style_scrollbar): Unit = !ptr._5 = v

      def cursor_normal_=(v: nk_color): Unit = !ptr._6 = v

      def cursor_hover_=(v: nk_color): Unit = !ptr._7 = v

      def cursor_text_normal_=(v: nk_color): Unit = !ptr._8 = v

      def cursor_text_hover_=(v: nk_color): Unit = !ptr._9 = v

      def text_normal_=(v: nk_color): Unit = !ptr._10 = v

      def text_hover_=(v: nk_color): Unit = !ptr._11 = v

      def text_active_=(v: nk_color): Unit = !ptr._12 = v

      def selected_normal_=(v: nk_color): Unit = !ptr._13 = v

      def selected_hover_=(v: nk_color): Unit = !ptr._14 = v

      def selected_text_normal_=(v: nk_color): Unit = !ptr._15 = v

      def selected_text_hover_=(v: nk_color): Unit = !ptr._16 = v

      def border_=(v: CFloat): Unit = !ptr._17 = v

      def rounding_=(v: CFloat): Unit = !ptr._18 = v

      def cursor_size_=(v: CFloat): Unit = !ptr._19 = v

      def scrollbar_size_=(v: nk_vec2): Unit = !ptr._20 = v

      def padding_=(v: nk_vec2): Unit = !ptr._21 = v

      def row_padding_=(v: CFloat): Unit = !ptr._22 = v
    }

    implicit class nk_style_propertyOps(val ptr: Ptr[nk_style_property]) extends AnyVal {
      def normal: nk_style_item = !ptr._1

      def hover: nk_style_item = !ptr._2

      def active: nk_style_item = !ptr._3

      def border_color: nk_color = !ptr._4

      def label_normal: nk_color = !ptr._5

      def label_hover: nk_color = !ptr._6

      def label_active: nk_color = !ptr._7

      def sym_left: nk_symbol_type = !ptr._8

      def sym_right: nk_symbol_type = !ptr._9

      def border: CFloat = !ptr._10

      def rounding: CFloat = !ptr._11

      def padding: nk_vec2 = !ptr._12

      def edit: nk_style_edit = !ptr._13

      def inc_button: nk_style_button = !ptr._14

      def dec_button: nk_style_button = !ptr._15

      def userdata: nk_handle = !ptr._16

      def draw_begin: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit] = !ptr._17

      def draw_end: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit] = !ptr._18

      def normal_=(v: nk_style_item): Unit = !ptr._1 = v

      def hover_=(v: nk_style_item): Unit = !ptr._2 = v

      def active_=(v: nk_style_item): Unit = !ptr._3 = v

      def border_color_=(v: nk_color): Unit = !ptr._4 = v

      def label_normal_=(v: nk_color): Unit = !ptr._5 = v

      def label_hover_=(v: nk_color): Unit = !ptr._6 = v

      def label_active_=(v: nk_color): Unit = !ptr._7 = v

      def sym_left_=(v: nk_symbol_type): Unit = !ptr._8 = v

      def sym_right_=(v: nk_symbol_type): Unit = !ptr._9 = v

      def border_=(v: CFloat): Unit = !ptr._10 = v

      def rounding_=(v: CFloat): Unit = !ptr._11 = v

      def padding_=(v: nk_vec2): Unit = !ptr._12 = v

      def edit_=(v: nk_style_edit): Unit = !ptr._13 = v

      def inc_button_=(v: nk_style_button): Unit = !ptr._14 = v

      def dec_button_=(v: nk_style_button): Unit = !ptr._15 = v

      def userdata_=(v: nk_handle): Unit = !ptr._16 = v

      def draw_begin_=(v: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]): Unit = !ptr._17 = v

      def draw_end_=(v: CFunctionPtr2[Ptr[nk_command_buffer], nk_handle, Unit]): Unit = !ptr._18 = v
    }

    implicit class nk_style_chartOps(val ptr: Ptr[nk_style_chart]) extends AnyVal {
      def background: nk_style_item = !ptr._1

      def border_color: nk_color = !ptr._2

      def selected_color: nk_color = !ptr._3

      def color: nk_color = !ptr._4

      def border: CFloat = !ptr._5

      def rounding: CFloat = !ptr._6

      def padding: nk_vec2 = !ptr._7

      def background_=(v: nk_style_item): Unit = !ptr._1 = v

      def border_color_=(v: nk_color): Unit = !ptr._2 = v

      def selected_color_=(v: nk_color): Unit = !ptr._3 = v

      def color_=(v: nk_color): Unit = !ptr._4 = v

      def border_=(v: CFloat): Unit = !ptr._5 = v

      def rounding_=(v: CFloat): Unit = !ptr._6 = v

      def padding_=(v: nk_vec2): Unit = !ptr._7 = v
    }

    implicit class nk_style_comboOps(val ptr: Ptr[nk_style_combo]) extends AnyVal {
      def normal: nk_style_item = !ptr._1

      def hover: nk_style_item = !ptr._2

      def active: nk_style_item = !ptr._3

      def border_color: nk_color = !ptr._4

      def label_normal: nk_color = !ptr._5

      def label_hover: nk_color = !ptr._6

      def label_active: nk_color = !ptr._7

      def symbol_normal: nk_color = !ptr._8

      def symbol_hover: nk_color = !ptr._9

      def symbol_active: nk_color = !ptr._10

      def button: nk_style_button = !ptr._11

      def sym_normal: nk_symbol_type = !ptr._12

      def sym_hover: nk_symbol_type = !ptr._13

      def sym_active: nk_symbol_type = !ptr._14

      def border: CFloat = !ptr._15

      def rounding: CFloat = !ptr._16

      def content_padding: nk_vec2 = !ptr._17

      def button_padding: nk_vec2 = !ptr._18

      def spacing: nk_vec2 = !ptr._19

      def normal_=(v: nk_style_item): Unit = !ptr._1 = v

      def hover_=(v: nk_style_item): Unit = !ptr._2 = v

      def active_=(v: nk_style_item): Unit = !ptr._3 = v

      def border_color_=(v: nk_color): Unit = !ptr._4 = v

      def label_normal_=(v: nk_color): Unit = !ptr._5 = v

      def label_hover_=(v: nk_color): Unit = !ptr._6 = v

      def label_active_=(v: nk_color): Unit = !ptr._7 = v

      def symbol_normal_=(v: nk_color): Unit = !ptr._8 = v

      def symbol_hover_=(v: nk_color): Unit = !ptr._9 = v

      def symbol_active_=(v: nk_color): Unit = !ptr._10 = v

      def button_=(v: nk_style_button): Unit = !ptr._11 = v

      def sym_normal_=(v: nk_symbol_type): Unit = !ptr._12 = v

      def sym_hover_=(v: nk_symbol_type): Unit = !ptr._13 = v

      def sym_active_=(v: nk_symbol_type): Unit = !ptr._14 = v

      def border_=(v: CFloat): Unit = !ptr._15 = v

      def rounding_=(v: CFloat): Unit = !ptr._16 = v

      def content_padding_=(v: nk_vec2): Unit = !ptr._17 = v

      def button_padding_=(v: nk_vec2): Unit = !ptr._18 = v

      def spacing_=(v: nk_vec2): Unit = !ptr._19 = v
    }

    implicit class nk_style_tabOps(val ptr: Ptr[nk_style_tab]) extends AnyVal {
      def background: nk_style_item = !ptr._1

      def border_color: nk_color = !ptr._2

      def text: nk_color = !ptr._3

      def tab_maximize_button: nk_style_button = !ptr._4

      def tab_minimize_button: nk_style_button = !ptr._5

      def node_maximize_button: nk_style_button = !ptr._6

      def node_minimize_button: nk_style_button = !ptr._7

      def sym_minimize: nk_symbol_type = !ptr._8

      def sym_maximize: nk_symbol_type = !ptr._9

      def border: CFloat = !ptr._10

      def rounding: CFloat = !ptr._11

      def indent: CFloat = !ptr._12

      def padding: nk_vec2 = !ptr._13

      def spacing: nk_vec2 = !ptr._14

      def background_=(v: nk_style_item): Unit = !ptr._1 = v

      def border_color_=(v: nk_color): Unit = !ptr._2 = v

      def text_=(v: nk_color): Unit = !ptr._3 = v

      def tab_maximize_button_=(v: nk_style_button): Unit = !ptr._4 = v

      def tab_minimize_button_=(v: nk_style_button): Unit = !ptr._5 = v

      def node_maximize_button_=(v: nk_style_button): Unit = !ptr._6 = v

      def node_minimize_button_=(v: nk_style_button): Unit = !ptr._7 = v

      def sym_minimize_=(v: nk_symbol_type): Unit = !ptr._8 = v

      def sym_maximize_=(v: nk_symbol_type): Unit = !ptr._9 = v

      def border_=(v: CFloat): Unit = !ptr._10 = v

      def rounding_=(v: CFloat): Unit = !ptr._11 = v

      def indent_=(v: CFloat): Unit = !ptr._12 = v

      def padding_=(v: nk_vec2): Unit = !ptr._13 = v

      def spacing_=(v: nk_vec2): Unit = !ptr._14 = v
    }

    object nk_style_header_align {
      nk_style_header_align
      nk_style_header_align
    }

    implicit class nk_style_window_headerOps(val ptr: Ptr[nk_style_window_header]) extends AnyVal {
      def normal: nk_style_item = !ptr._1

      def hover: nk_style_item = !ptr._2

      def active: nk_style_item = !ptr._3

      def close_button: nk_style_button = !ptr._4

      def minimize_button: nk_style_button = !ptr._5

      def close_symbol: nk_symbol_type = !ptr._6

      def minimize_symbol: nk_symbol_type = !ptr._7

      def maximize_symbol: nk_symbol_type = !ptr._8

      def label_normal: nk_color = !ptr._9

      def label_hover: nk_color = !ptr._10

      def label_active: nk_color = !ptr._11

      def align: nk_style_header_align = !ptr._12

      def padding: nk_vec2 = !ptr._13

      def label_padding: nk_vec2 = !ptr._14

      def spacing: nk_vec2 = !ptr._15

      def normal_=(v: nk_style_item): Unit = !ptr._1 = v

      def hover_=(v: nk_style_item): Unit = !ptr._2 = v

      def active_=(v: nk_style_item): Unit = !ptr._3 = v

      def close_button_=(v: nk_style_button): Unit = !ptr._4 = v

      def minimize_button_=(v: nk_style_button): Unit = !ptr._5 = v

      def close_symbol_=(v: nk_symbol_type): Unit = !ptr._6 = v

      def minimize_symbol_=(v: nk_symbol_type): Unit = !ptr._7 = v

      def maximize_symbol_=(v: nk_symbol_type): Unit = !ptr._8 = v

      def label_normal_=(v: nk_color): Unit = !ptr._9 = v

      def label_hover_=(v: nk_color): Unit = !ptr._10 = v

      def label_active_=(v: nk_color): Unit = !ptr._11 = v

      def align_=(v: nk_style_header_align): Unit = !ptr._12 = v

      def padding_=(v: nk_vec2): Unit = !ptr._13 = v

      def label_padding_=(v: nk_vec2): Unit = !ptr._14 = v

      def spacing_=(v: nk_vec2): Unit = !ptr._15 = v
    }

    implicit class nk_style_windowOps(val ptr: Ptr[nk_style_window]) extends AnyVal {
      def header: nk_style_window_header = !ptr._1

      def fixed_background: nk_style_item = !ptr._2

      def background: nk_color = !ptr._3

      def border_color: nk_color = !ptr._4

      def popup_border_color: nk_color = !ptr._5

      def combo_border_color: nk_color = !ptr._6

      def contextual_border_color: nk_color = !ptr._7

      def menu_border_color: nk_color = !ptr._8

      def group_border_color: nk_color = !ptr._9

      def tooltip_border_color: nk_color = !ptr._10

      def scaler: nk_style_item = !ptr._11

      def border: CFloat = !ptr._12

      def combo_border: CFloat = !ptr._13

      def contextual_border: CFloat = !ptr._14

      def menu_border: CFloat = !ptr._15

      def group_border: CFloat = !ptr._16

      def tooltip_border: CFloat = !ptr._17

      def popup_border: CFloat = !ptr._18

      def min_row_height_padding: CFloat = !ptr._19

      def rounding: CFloat = !ptr._20

      def spacing: nk_vec2 = !ptr._21

      def scrollbar_size: nk_vec2 = !ptr._22

      def min_size: nk_vec2 = !ptr._23

      def padding: nk_vec2 = !ptr._24

      def group_padding: nk_vec2 = !ptr._25

      def popup_padding: nk_vec2 = !ptr._26

      def combo_padding: nk_vec2 = !ptr._27

      def contextual_padding: nk_vec2 = !ptr._28

      def menu_padding: nk_vec2 = !ptr._29

      def tooltip_padding: nk_vec2 = !ptr._30

      def header_=(v: nk_style_window_header): Unit = !ptr._1 = v

      def fixed_background_=(v: nk_style_item): Unit = !ptr._2 = v

      def background_=(v: nk_color): Unit = !ptr._3 = v

      def border_color_=(v: nk_color): Unit = !ptr._4 = v

      def popup_border_color_=(v: nk_color): Unit = !ptr._5 = v

      def combo_border_color_=(v: nk_color): Unit = !ptr._6 = v

      def contextual_border_color_=(v: nk_color): Unit = !ptr._7 = v

      def menu_border_color_=(v: nk_color): Unit = !ptr._8 = v

      def group_border_color_=(v: nk_color): Unit = !ptr._9 = v

      def tooltip_border_color_=(v: nk_color): Unit = !ptr._10 = v

      def scaler_=(v: nk_style_item): Unit = !ptr._11 = v

      def border_=(v: CFloat): Unit = !ptr._12 = v

      def combo_border_=(v: CFloat): Unit = !ptr._13 = v

      def contextual_border_=(v: CFloat): Unit = !ptr._14 = v

      def menu_border_=(v: CFloat): Unit = !ptr._15 = v

      def group_border_=(v: CFloat): Unit = !ptr._16 = v

      def tooltip_border_=(v: CFloat): Unit = !ptr._17 = v

      def popup_border_=(v: CFloat): Unit = !ptr._18 = v

      def min_row_height_padding_=(v: CFloat): Unit = !ptr._19 = v

      def rounding_=(v: CFloat): Unit = !ptr._20 = v

      def spacing_=(v: nk_vec2): Unit = !ptr._21 = v

      def scrollbar_size_=(v: nk_vec2): Unit = !ptr._22 = v

      def min_size_=(v: nk_vec2): Unit = !ptr._23 = v

      def padding_=(v: nk_vec2): Unit = !ptr._24 = v

      def group_padding_=(v: nk_vec2): Unit = !ptr._25 = v

      def popup_padding_=(v: nk_vec2): Unit = !ptr._26 = v

      def combo_padding_=(v: nk_vec2): Unit = !ptr._27 = v

      def contextual_padding_=(v: nk_vec2): Unit = !ptr._28 = v

      def menu_padding_=(v: nk_vec2): Unit = !ptr._29 = v

      def tooltip_padding_=(v: nk_vec2): Unit = !ptr._30 = v
    }

    implicit class nk_styleOps(val ptr: Ptr[nk_style]) extends AnyVal {
      def font: Ptr[nk_user_font] = !ptr._1

      def cursors: Ptr[Ptr[nk_cursor]] = !ptr._2

      def cursor_active: Ptr[nk_cursor] = !ptr._3

      def cursor_last: Ptr[nk_cursor] = !ptr._4

      def cursor_visible: CInt = !ptr._5

      def text: nk_style_text = !ptr._6

      def button: nk_style_button = !ptr._7

      def contextual_button: nk_style_button = !ptr._8

      def menu_button: nk_style_button = !ptr._9

      def option: nk_style_toggle = !ptr._10

      def checkbox: nk_style_toggle = !ptr._11

      def selectable: nk_style_selectable = !ptr._12

      def slider: nk_style_slider = !ptr._13

      def progress: nk_style_progress = !ptr._14

      def property: nk_style_property = !ptr._15

      def edit: nk_style_edit = !ptr._16

      def chart: nk_style_chart = !ptr._17

      def scrollh: nk_style_scrollbar = !ptr._18

      def scrollv: nk_style_scrollbar = !ptr._19

      def tab: nk_style_tab = !ptr._20

      def combo: nk_style_combo = !ptr._21

      def window: nk_style_window = !ptr._22

      def font_=(v: Ptr[nk_user_font]): Unit = !ptr._1 = v

      def cursors_=(v: Ptr[Ptr[nk_cursor]]): Unit = !ptr._2 = v

      def cursor_active_=(v: Ptr[nk_cursor]): Unit = !ptr._3 = v

      def cursor_last_=(v: Ptr[nk_cursor]): Unit = !ptr._4 = v

      def cursor_visible_=(v: CInt): Unit = !ptr._5 = v

      def text_=(v: nk_style_text): Unit = !ptr._6 = v

      def button_=(v: nk_style_button): Unit = !ptr._7 = v

      def contextual_button_=(v: nk_style_button): Unit = !ptr._8 = v

      def menu_button_=(v: nk_style_button): Unit = !ptr._9 = v

      def option_=(v: nk_style_toggle): Unit = !ptr._10 = v

      def checkbox_=(v: nk_style_toggle): Unit = !ptr._11 = v

      def selectable_=(v: nk_style_selectable): Unit = !ptr._12 = v

      def slider_=(v: nk_style_slider): Unit = !ptr._13 = v

      def progress_=(v: nk_style_progress): Unit = !ptr._14 = v

      def property_=(v: nk_style_property): Unit = !ptr._15 = v

      def edit_=(v: nk_style_edit): Unit = !ptr._16 = v

      def chart_=(v: nk_style_chart): Unit = !ptr._17 = v

      def scrollh_=(v: nk_style_scrollbar): Unit = !ptr._18 = v

      def scrollv_=(v: nk_style_scrollbar): Unit = !ptr._19 = v

      def tab_=(v: nk_style_tab): Unit = !ptr._20 = v

      def combo_=(v: nk_style_combo): Unit = !ptr._21 = v

      def window_=(v: nk_style_window): Unit = !ptr._22 = v
    }

    object nk_panel_type {
      nk_panel_type
      nk_panel_type
      nk_panel_type
      nk_panel_type
      nk_panel_type
      nk_panel_type
      nk_panel_type
    }

    object nk_panel_set {
      nk_panel_set
      nk_panel_set
      nk_panel_set
    }

    implicit class nk_chart_slotOps(val ptr: Ptr[nk_chart_slot]) extends AnyVal {
      def `type`: nk_chart_type = !ptr._1

      def color: nk_color = !ptr._2

      def highlight: nk_color = !ptr._3

      def min: CFloat = !ptr._4

      def max: CFloat = !ptr._5

      def range: CFloat = !ptr._6

      def count: CInt = !ptr._7

      def last: nk_vec2 = !ptr._8

      def index: CInt = !ptr._9

      def type_=(v: nk_chart_type): Unit = !ptr._1 = v

      def color_=(v: nk_color): Unit = !ptr._2 = v

      def highlight_=(v: nk_color): Unit = !ptr._3 = v

      def min_=(v: CFloat): Unit = !ptr._4 = v

      def max_=(v: CFloat): Unit = !ptr._5 = v

      def range_=(v: CFloat): Unit = !ptr._6 = v

      def count_=(v: CInt): Unit = !ptr._7 = v

      def last_=(v: nk_vec2): Unit = !ptr._8 = v

      def index_=(v: CInt): Unit = !ptr._9 = v
    }

    implicit class nk_chartOps(val ptr: Ptr[nk_chart]) extends AnyVal {
      def slot: CInt = !ptr._1

      def x: CFloat = !ptr._2

      def y: CFloat = !ptr._3

      def w: CFloat = !ptr._4

      def h: CFloat = !ptr._5

      def slots: Ptr[nk_chart_slot] = !ptr._6

      def slot_=(v: CInt): Unit = !ptr._1 = v

      def x_=(v: CFloat): Unit = !ptr._2 = v

      def y_=(v: CFloat): Unit = !ptr._3 = v

      def w_=(v: CFloat): Unit = !ptr._4 = v

      def h_=(v: CFloat): Unit = !ptr._5 = v

      def slots_=(v: Ptr[nk_chart_slot]): Unit = !ptr._6 = v
    }

    object nk_panel_row_layout_type {
      nk_panel_row_layout_type
      nk_panel_row_layout_type
      nk_panel_row_layout_type
      nk_panel_row_layout_type
      nk_panel_row_layout_type
      nk_panel_row_layout_type
      nk_panel_row_layout_type
      nk_panel_row_layout_type
      nk_panel_row_layout_type
      nk_panel_row_layout_type
    }

    implicit class nk_row_layoutOps(val ptr: Ptr[nk_row_layout]) extends AnyVal {
      def `type`: nk_panel_row_layout_type = !ptr._1

      def index: CInt = !ptr._2

      def height: CFloat = !ptr._3

      def min_height: CFloat = !ptr._4

      def columns: CInt = !ptr._5

      def ratio: Ptr[CFloat] = !ptr._6

      def item_width: CFloat = !ptr._7

      def item_height: CFloat = !ptr._8

      def item_offset: CFloat = !ptr._9

      def filled: CFloat = !ptr._10

      def item: nk_rect = !ptr._11

      def tree_depth: CInt = !ptr._12

      def templates: Ptr[CFloat] = !ptr._13

      def type_=(v: nk_panel_row_layout_type): Unit = !ptr._1 = v

      def index_=(v: CInt): Unit = !ptr._2 = v

      def height_=(v: CFloat): Unit = !ptr._3 = v

      def min_height_=(v: CFloat): Unit = !ptr._4 = v

      def columns_=(v: CInt): Unit = !ptr._5 = v

      def ratio_=(v: Ptr[CFloat]): Unit = !ptr._6 = v

      def item_width_=(v: CFloat): Unit = !ptr._7 = v

      def item_height_=(v: CFloat): Unit = !ptr._8 = v

      def item_offset_=(v: CFloat): Unit = !ptr._9 = v

      def filled_=(v: CFloat): Unit = !ptr._10 = v

      def item_=(v: nk_rect): Unit = !ptr._11 = v

      def tree_depth_=(v: CInt): Unit = !ptr._12 = v

      def templates_=(v: Ptr[CFloat]): Unit = !ptr._13 = v
    }

    implicit class nk_popup_bufferOps(val ptr: Ptr[nk_popup_buffer]) extends AnyVal {
      def begin: nk_size = !ptr._1

      def parent: nk_size = !ptr._2

      def last: nk_size = !ptr._3

      def end: nk_size = !ptr._4

      def active: CInt = !ptr._5

      def begin_=(v: nk_size): Unit = !ptr._1 = v

      def parent_=(v: nk_size): Unit = !ptr._2 = v

      def last_=(v: nk_size): Unit = !ptr._3 = v

      def end_=(v: nk_size): Unit = !ptr._4 = v

      def active_=(v: CInt): Unit = !ptr._5 = v
    }

    implicit class nk_menu_stateOps(val ptr: Ptr[nk_menu_state]) extends AnyVal {
      def x: CFloat = !ptr._1

      def y: CFloat = !ptr._2

      def w: CFloat = !ptr._3

      def h: CFloat = !ptr._4

      def offset: nk_scroll = !ptr._5

      def x_=(v: CFloat): Unit = !ptr._1 = v

      def y_=(v: CFloat): Unit = !ptr._2 = v

      def w_=(v: CFloat): Unit = !ptr._3 = v

      def h_=(v: CFloat): Unit = !ptr._4 = v

      def offset_=(v: nk_scroll): Unit = !ptr._5 = v
    }

    implicit class nk_panelOps(val ptr: Ptr[nk_panel]) extends AnyVal {
      def `type`: nk_panel_type = !ptr._1

      def flags: nk_flags = !ptr._2

      def bounds: nk_rect = !ptr._3

      def offset_x: Ptr[nk_uint] = !ptr._4

      def offset_y: Ptr[nk_uint] = !ptr._5

      def at_x: CFloat = !ptr._6

      def at_y: CFloat = !ptr._7

      def max_x: CFloat = !ptr._8

      def footer_height: CFloat = !ptr._9

      def header_height: CFloat = !ptr._10

      def border: CFloat = !ptr._11

      def has_scrolling: CUnsignedInt = !ptr._12

      def clip: nk_rect = !ptr._13

      def menu: nk_menu_state = !ptr._14

      def row: nk_row_layout = !ptr._15

      def chart: nk_chart = !ptr._16

      def buffer: Ptr[nk_command_buffer] = !ptr._17

      def parent: Ptr[nk_panel] = !ptr._18

      def type_=(v: nk_panel_type): Unit = !ptr._1 = v

      def flags_=(v: nk_flags): Unit = !ptr._2 = v

      def bounds_=(v: nk_rect): Unit = !ptr._3 = v

      def offset_x_=(v: Ptr[nk_uint]): Unit = !ptr._4 = v

      def offset_y_=(v: Ptr[nk_uint]): Unit = !ptr._5 = v

      def at_x_=(v: CFloat): Unit = !ptr._6 = v

      def at_y_=(v: CFloat): Unit = !ptr._7 = v

      def max_x_=(v: CFloat): Unit = !ptr._8 = v

      def footer_height_=(v: CFloat): Unit = !ptr._9 = v

      def header_height_=(v: CFloat): Unit = !ptr._10 = v

      def border_=(v: CFloat): Unit = !ptr._11 = v

      def has_scrolling_=(v: CUnsignedInt): Unit = !ptr._12 = v

      def clip_=(v: nk_rect): Unit = !ptr._13 = v

      def menu_=(v: nk_menu_state): Unit = !ptr._14 = v

      def row_=(v: nk_row_layout): Unit = !ptr._15 = v

      def chart_=(v: nk_chart): Unit = !ptr._16 = v

      def buffer_=(v: Ptr[nk_command_buffer]): Unit = !ptr._17 = v

      def parent_=(v: Ptr[nk_panel]): Unit = !ptr._18 = v
    }

    object nk_window_flags {
      nk_window_flags
      nk_window_flags
      nk_window_flags
      nk_window_flags
      nk_window_flags
      nk_window_flags
      nk_window_flags
      nk_window_flags
    }

    implicit class nk_popup_stateOps(val ptr: Ptr[nk_popup_state]) extends AnyVal {
      def win: Ptr[nk_window] = !ptr._1

      def `type`: nk_panel_type = !ptr._2

      def buf: nk_popup_buffer = !ptr._3

      def name: nk_hash = !ptr._4

      def active: CInt = !ptr._5

      def combo_count: CUnsignedInt = !ptr._6

      def con_count: CUnsignedInt = !ptr._7

      def con_old: CUnsignedInt = !ptr._8

      def active_con: CUnsignedInt = !ptr._9

      def header: nk_rect = !ptr._10

      def win_=(v: Ptr[nk_window]): Unit = !ptr._1 = v

      def type_=(v: nk_panel_type): Unit = !ptr._2 = v

      def buf_=(v: nk_popup_buffer): Unit = !ptr._3 = v

      def name_=(v: nk_hash): Unit = !ptr._4 = v

      def active_=(v: CInt): Unit = !ptr._5 = v

      def combo_count_=(v: CUnsignedInt): Unit = !ptr._6 = v

      def con_count_=(v: CUnsignedInt): Unit = !ptr._7 = v

      def con_old_=(v: CUnsignedInt): Unit = !ptr._8 = v

      def active_con_=(v: CUnsignedInt): Unit = !ptr._9 = v

      def header_=(v: nk_rect): Unit = !ptr._10 = v
    }

    implicit class nk_edit_stateOps(val ptr: Ptr[nk_edit_state]) extends AnyVal {
      def name: nk_hash = !ptr._1

      def seq: CUnsignedInt = !ptr._2

      def old: CUnsignedInt = !ptr._3

      def active: CInt = !ptr._4

      def prev: CInt = !ptr._5

      def cursor: CInt = !ptr._6

      def sel_start: CInt = !ptr._7

      def sel_end: CInt = !ptr._8

      def scrollbar: nk_scroll = !ptr._9

      def mode: CUnsignedChar = !ptr._10

      def single_line: CUnsignedChar = !ptr._11

      def name_=(v: nk_hash): Unit = !ptr._1 = v

      def seq_=(v: CUnsignedInt): Unit = !ptr._2 = v

      def old_=(v: CUnsignedInt): Unit = !ptr._3 = v

      def active_=(v: CInt): Unit = !ptr._4 = v

      def prev_=(v: CInt): Unit = !ptr._5 = v

      def cursor_=(v: CInt): Unit = !ptr._6 = v

      def sel_start_=(v: CInt): Unit = !ptr._7 = v

      def sel_end_=(v: CInt): Unit = !ptr._8 = v

      def scrollbar_=(v: nk_scroll): Unit = !ptr._9 = v

      def mode_=(v: CUnsignedChar): Unit = !ptr._10 = v

      def single_line_=(v: CUnsignedChar): Unit = !ptr._11 = v
    }

    implicit class nk_property_stateOps(val ptr: Ptr[nk_property_state]) extends AnyVal {
      def active: CInt = !ptr._1

      def prev: CInt = !ptr._2

      def buffer: CString = !ptr._3

      def length: CInt = !ptr._4

      def cursor: CInt = !ptr._5

      def select_start: CInt = !ptr._6

      def select_end: CInt = !ptr._7

      def name: nk_hash = !ptr._8

      def seq: CUnsignedInt = !ptr._9

      def old: CUnsignedInt = !ptr._10

      def state: CInt = !ptr._11

      def active_=(v: CInt): Unit = !ptr._1 = v

      def prev_=(v: CInt): Unit = !ptr._2 = v

      def buffer_=(v: CString): Unit = !ptr._3 = v

      def length_=(v: CInt): Unit = !ptr._4 = v

      def cursor_=(v: CInt): Unit = !ptr._5 = v

      def select_start_=(v: CInt): Unit = !ptr._6 = v

      def select_end_=(v: CInt): Unit = !ptr._7 = v

      def name_=(v: nk_hash): Unit = !ptr._8 = v

      def seq_=(v: CUnsignedInt): Unit = !ptr._9 = v

      def old_=(v: CUnsignedInt): Unit = !ptr._10 = v

      def state_=(v: CInt): Unit = !ptr._11 = v
    }

    implicit class nk_windowOps(val ptr: Ptr[nk_window]) extends AnyVal {
      def seq: CUnsignedInt = !ptr._1

      def name: nk_hash = !ptr._2

      def name_string: CString = !ptr._3

      def flags: nk_flags = !ptr._4

      def bounds: nk_rect = !ptr._5

      def scrollbar: nk_scroll = !ptr._6

      def buffer: nk_command_buffer = !ptr._7

      def layout: Ptr[nk_panel] = !ptr._8

      def scrollbar_hiding_timer: CFloat = !ptr._9

      def property: nk_property_state = !ptr._10

      def popup: nk_popup_state = !ptr._11

      def edit: nk_edit_state = !ptr._12

      def scrolled: CUnsignedInt = !ptr._13

      def tables: Ptr[nk_table] = !ptr._14

      def table_count: CUnsignedInt = !ptr._15

      def next: Ptr[nk_window] = !ptr._16

      def prev: Ptr[nk_window] = !ptr._17

      def parent: Ptr[nk_window] = !ptr._18

      def seq_=(v: CUnsignedInt): Unit = !ptr._1 = v

      def name_=(v: nk_hash): Unit = !ptr._2 = v

      def name_string_=(v: CString): Unit = !ptr._3 = v

      def flags_=(v: nk_flags): Unit = !ptr._4 = v

      def bounds_=(v: nk_rect): Unit = !ptr._5 = v

      def scrollbar_=(v: nk_scroll): Unit = !ptr._6 = v

      def buffer_=(v: nk_command_buffer): Unit = !ptr._7 = v

      def layout_=(v: Ptr[nk_panel]): Unit = !ptr._8 = v

      def scrollbar_hiding_timer_=(v: CFloat): Unit = !ptr._9 = v

      def property_=(v: nk_property_state): Unit = !ptr._10 = v

      def popup_=(v: nk_popup_state): Unit = !ptr._11 = v

      def edit_=(v: nk_edit_state): Unit = !ptr._12 = v

      def scrolled_=(v: CUnsignedInt): Unit = !ptr._13 = v

      def tables_=(v: Ptr[nk_table]): Unit = !ptr._14 = v

      def table_count_=(v: CUnsignedInt): Unit = !ptr._15 = v

      def next_=(v: Ptr[nk_window]): Unit = !ptr._16 = v

      def prev_=(v: Ptr[nk_window]): Unit = !ptr._17 = v

      def parent_=(v: Ptr[nk_window]): Unit = !ptr._18 = v
    }

    implicit class nk_config_stack_style_item_elementOps(val ptr: Ptr[nk_config_stack_style_item_element]) extends AnyVal {
      def address: Ptr[nk_style_item] = !ptr._1

      def old_value: nk_style_item = !ptr._2

      def address_=(v: Ptr[nk_style_item]): Unit = !ptr._1 = v

      def old_value_=(v: nk_style_item): Unit = !ptr._2 = v
    }

    implicit class nk_config_stack_float_elementOps(val ptr: Ptr[nk_config_stack_float_element]) extends AnyVal {
      def address: Ptr[CFloat] = !ptr._1

      def old_value: CFloat = !ptr._2

      def address_=(v: Ptr[CFloat]): Unit = !ptr._1 = v

      def old_value_=(v: CFloat): Unit = !ptr._2 = v
    }

    implicit class nk_config_stack_vec2_elementOps(val ptr: Ptr[nk_config_stack_vec2_element]) extends AnyVal {
      def address: Ptr[nk_vec2] = !ptr._1

      def old_value: nk_vec2 = !ptr._2

      def address_=(v: Ptr[nk_vec2]): Unit = !ptr._1 = v

      def old_value_=(v: nk_vec2): Unit = !ptr._2 = v
    }

    implicit class nk_config_stack_flags_elementOps(val ptr: Ptr[nk_config_stack_flags_element]) extends AnyVal {
      def address: Ptr[nk_flags] = !ptr._1

      def old_value: nk_flags = !ptr._2

      def address_=(v: Ptr[nk_flags]): Unit = !ptr._1 = v

      def old_value_=(v: nk_flags): Unit = !ptr._2 = v
    }

    implicit class nk_config_stack_color_elementOps(val ptr: Ptr[nk_config_stack_color_element]) extends AnyVal {
      def address: Ptr[nk_color] = !ptr._1

      def old_value: nk_color = !ptr._2

      def address_=(v: Ptr[nk_color]): Unit = !ptr._1 = v

      def old_value_=(v: nk_color): Unit = !ptr._2 = v
    }

    implicit class nk_config_stack_user_font_elementOps(val ptr: Ptr[nk_config_stack_user_font_element]) extends AnyVal {
      def address: Ptr[Ptr[nk_user_font]] = !ptr._1

      def old_value: Ptr[nk_user_font] = !ptr._2

      def address_=(v: Ptr[Ptr[nk_user_font]]): Unit = !ptr._1 = v

      def old_value_=(v: Ptr[nk_user_font]): Unit = !ptr._2 = v
    }

    implicit class nk_config_stack_button_behavior_elementOps(val ptr: Ptr[nk_config_stack_button_behavior_element]) extends AnyVal {
      def address: Ptr[nk_button_behavior] = !ptr._1

      def old_value: nk_button_behavior = !ptr._2

      def address_=(v: Ptr[nk_button_behavior]): Unit = !ptr._1 = v

      def old_value_=(v: nk_button_behavior): Unit = !ptr._2 = v
    }

    implicit class nk_config_stack_style_itemOps(val ptr: Ptr[nk_config_stack_style_item]) extends AnyVal {
      def head: CInt = !ptr._1

      def elements: Ptr[nk_config_stack_style_item_element] = !ptr._2

      def head_=(v: CInt): Unit = !ptr._1 = v

      def elements_=(v: Ptr[nk_config_stack_style_item_element]): Unit = !ptr._2 = v
    }

    implicit class nk_config_stack_floatOps(val ptr: Ptr[nk_config_stack_float]) extends AnyVal {
      def head: CInt = !ptr._1

      def elements: Ptr[nk_config_stack_float_element] = !ptr._2

      def head_=(v: CInt): Unit = !ptr._1 = v

      def elements_=(v: Ptr[nk_config_stack_float_element]): Unit = !ptr._2 = v
    }

    implicit class nk_config_stack_vec2Ops(val ptr: Ptr[nk_config_stack_vec2]) extends AnyVal {
      def head: CInt = !ptr._1

      def elements: Ptr[nk_config_stack_vec2_element] = !ptr._2

      def head_=(v: CInt): Unit = !ptr._1 = v

      def elements_=(v: Ptr[nk_config_stack_vec2_element]): Unit = !ptr._2 = v
    }

    implicit class nk_config_stack_flagsOps(val ptr: Ptr[nk_config_stack_flags]) extends AnyVal {
      def head: CInt = !ptr._1

      def elements: Ptr[nk_config_stack_flags_element] = !ptr._2

      def head_=(v: CInt): Unit = !ptr._1 = v

      def elements_=(v: Ptr[nk_config_stack_flags_element]): Unit = !ptr._2 = v
    }

    implicit class nk_config_stack_colorOps(val ptr: Ptr[nk_config_stack_color]) extends AnyVal {
      def head: CInt = !ptr._1

      def elements: Ptr[nk_config_stack_color_element] = !ptr._2

      def head_=(v: CInt): Unit = !ptr._1 = v

      def elements_=(v: Ptr[nk_config_stack_color_element]): Unit = !ptr._2 = v
    }

    implicit class nk_config_stack_user_fontOps(val ptr: Ptr[nk_config_stack_user_font]) extends AnyVal {
      def head: CInt = !ptr._1

      def elements: Ptr[nk_config_stack_user_font_element] = !ptr._2

      def head_=(v: CInt): Unit = !ptr._1 = v

      def elements_=(v: Ptr[nk_config_stack_user_font_element]): Unit = !ptr._2 = v
    }

    implicit class nk_config_stack_button_behaviorOps(val ptr: Ptr[nk_config_stack_button_behavior]) extends AnyVal {
      def head: CInt = !ptr._1

      def elements: Ptr[nk_config_stack_button_behavior_element] = !ptr._2

      def head_=(v: CInt): Unit = !ptr._1 = v

      def elements_=(v: Ptr[nk_config_stack_button_behavior_element]): Unit = !ptr._2 = v
    }

    implicit class nk_configuration_stacksOps(val ptr: Ptr[nk_configuration_stacks]) extends AnyVal {
      def style_items: nk_config_stack_style_item = !ptr._1

      def floats: nk_config_stack_float = !ptr._2

      def vectors: nk_config_stack_vec2 = !ptr._3

      def flags: nk_config_stack_flags = !ptr._4

      def colors: nk_config_stack_color = !ptr._5

      def fonts: nk_config_stack_user_font = !ptr._6

      def button_behaviors: nk_config_stack_button_behavior = !ptr._7

      def style_items_=(v: nk_config_stack_style_item): Unit = !ptr._1 = v

      def floats_=(v: nk_config_stack_float): Unit = !ptr._2 = v

      def vectors_=(v: nk_config_stack_vec2): Unit = !ptr._3 = v

      def flags_=(v: nk_config_stack_flags): Unit = !ptr._4 = v

      def colors_=(v: nk_config_stack_color): Unit = !ptr._5 = v

      def fonts_=(v: nk_config_stack_user_font): Unit = !ptr._6 = v

      def button_behaviors_=(v: nk_config_stack_button_behavior): Unit = !ptr._7 = v
    }

    implicit class nk_tableOps(val ptr: Ptr[nk_table]) extends AnyVal {
      def seq: CUnsignedInt = !ptr._1

      def size: CUnsignedInt = !ptr._2

      def keys: Ptr[nk_hash] = !ptr._3

      def values: Ptr[nk_uint] = !ptr._4

      def next: Ptr[nk_table] = !ptr._5

      def prev: Ptr[nk_table] = !ptr._6

      def seq_=(v: CUnsignedInt): Unit = !ptr._1 = v

      def size_=(v: CUnsignedInt): Unit = !ptr._2 = v

      def keys_=(v: Ptr[nk_hash]): Unit = !ptr._3 = v

      def values_=(v: Ptr[nk_uint]): Unit = !ptr._4 = v

      def next_=(v: Ptr[nk_table]): Unit = !ptr._5 = v

      def prev_=(v: Ptr[nk_table]): Unit = !ptr._6 = v
    }

    implicit class nk_pageOps(val ptr: Ptr[nk_page]) extends AnyVal {
      def size: CUnsignedInt = !ptr._1

      def next: Ptr[nk_page] = !ptr._2

      def win: Ptr[nk_page_element] = !ptr._3

      def size_=(v: CUnsignedInt): Unit = !ptr._1 = v

      def next_=(v: Ptr[nk_page]): Unit = !ptr._2 = v

      def win_=(v: Ptr[nk_page_element]): Unit = !ptr._3 = v
    }

    implicit class nk_poolOps(val ptr: Ptr[nk_pool]) extends AnyVal {
      def alloc: nk_allocator = !ptr._1

      def `type`: nk_allocation_type = !ptr._2

      def page_count: CUnsignedInt = !ptr._3

      def pages: Ptr[nk_page] = !ptr._4

      def freelist: Ptr[nk_page_element] = !ptr._5

      def capacity: CUnsignedInt = !ptr._6

      def size: nk_size = !ptr._7

      def cap: nk_size = !ptr._8

      def alloc_=(v: nk_allocator): Unit = !ptr._1 = v

      def type_=(v: nk_allocation_type): Unit = !ptr._2 = v

      def page_count_=(v: CUnsignedInt): Unit = !ptr._3 = v

      def pages_=(v: Ptr[nk_page]): Unit = !ptr._4 = v

      def freelist_=(v: Ptr[nk_page_element]): Unit = !ptr._5 = v

      def capacity_=(v: CUnsignedInt): Unit = !ptr._6 = v

      def size_=(v: nk_size): Unit = !ptr._7 = v

      def cap_=(v: nk_size): Unit = !ptr._8 = v
    }

    implicit class nk_contextOps(val ptr: Ptr[nk_context]) extends AnyVal {
      def input: nk_input = !ptr._1

      def style: nk_style = !ptr._2

      def memory: nk_buffer = !ptr._3

      def clip: nk_clipboard = !ptr._4

      def last_widget_state: nk_flags = !ptr._5

      def button_behavior: nk_button_behavior = !ptr._6

      def stacks: nk_configuration_stacks = !ptr._7

      def delta_time_seconds: CFloat = !ptr._8

      def text_edit: nk_text_edit = !ptr._9

      def overlay: nk_command_buffer = !ptr._10

      def build: CInt = !ptr._11

      def use_pool: CInt = !ptr._12

      def pool: nk_pool = !ptr._13

      def begin: Ptr[nk_window] = !ptr._14

      def end: Ptr[nk_window] = !ptr._15

      def active: Ptr[nk_window] = !ptr._16

      def current: Ptr[nk_window] = !ptr._17

      def freelist: Ptr[nk_page_element] = !ptr._18

      def count: CUnsignedInt = !ptr._19

      def seq: CUnsignedInt = !ptr._20

      def input_=(v: nk_input): Unit = !ptr._1 = v

      def style_=(v: nk_style): Unit = !ptr._2 = v

      def memory_=(v: nk_buffer): Unit = !ptr._3 = v

      def clip_=(v: nk_clipboard): Unit = !ptr._4 = v

      def last_widget_state_=(v: nk_flags): Unit = !ptr._5 = v

      def button_behavior_=(v: nk_button_behavior): Unit = !ptr._6 = v

      def stacks_=(v: nk_configuration_stacks): Unit = !ptr._7 = v

      def delta_time_seconds_=(v: CFloat): Unit = !ptr._8 = v

      def text_edit_=(v: nk_text_edit): Unit = !ptr._9 = v

      def overlay_=(v: nk_command_buffer): Unit = !ptr._10 = v

      def build_=(v: CInt): Unit = !ptr._11 = v

      def use_pool_=(v: CInt): Unit = !ptr._12 = v

      def pool_=(v: nk_pool): Unit = !ptr._13 = v

      def begin_=(v: Ptr[nk_window]): Unit = !ptr._14 = v

      def end_=(v: Ptr[nk_window]): Unit = !ptr._15 = v

      def active_=(v: Ptr[nk_window]): Unit = !ptr._16 = v

      def current_=(v: Ptr[nk_window]): Unit = !ptr._17 = v

      def freelist_=(v: Ptr[nk_page_element]): Unit = !ptr._18 = v

      def count_=(v: CUnsignedInt): Unit = !ptr._19 = v

      def seq_=(v: CUnsignedInt): Unit = !ptr._20 = v
    }

  }

}
