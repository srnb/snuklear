import java.io.FileWriter
import java.nio.file.Files

import scala.sys.process._

enablePlugins(ScalaNativePlugin)

val gccHome = settingKey[String]("Where the GCC/cc1/library home is")
val gccPath = settingKey[String]("Where GCC is located")
val gitPath = settingKey[String]("Where Git is located")
val arPath = settingKey[String]("Where AR is located")
val nuklearLibraryOutput = settingKey[String]("Where the .a file should be placed")
val nuklearBuildFlags = settingKey[Seq[String]]("Build flags for nuklear.c")
val compileNuklear = taskKey[Unit]("Compile the library into a .a file")

lazy val settings = Seq(
  gccHome := "/usr/lib/gcc/x86_64-pc-linux-gnu/7.3.1",
  gccPath := "/usr/bin/gcc",
  gitPath := "/usr/bin/git",
  arPath := "/usr/bin/ar",
  nuklearBuildFlags := "NK_INCLUDE_FIXED_TYPES" :: "NK_INCLUDE_DEFAULT_ALLOCATOR" :: Nil,
  nuklearLibraryOutput := "src/main/resources/libnuklear.a"
)

lazy val root = (project in file(".")).settings(
  name := "snuklear",
  version := "0.1",
  scalaVersion := "2.11.12",
  settings,
  compileNuklear := {
    val td = Files.createTempDirectory(s"sbtnuklear${System.nanoTime()}").toFile
    val cd = baseDirectory.value
    Process(gitPath.value :: "submodule" :: "update" :: Nil, cd).!
    val cf = new File(td, "nuklear.c")
    val fw = new FileWriter(cf)
    fw.write(
      s"""
        |#define NK_IMPLEMENTATION
        |${nuklearBuildFlags.value.map((f) => s"#define $f").mkString("\n")}
        |#include "${new File(cd, "nuklear/nuklear.h").getAbsolutePath}"
      """.stripMargin)
    fw.close()
    val of = new File(td, "nuklear.o")
    s"${gccPath.value} -B ${gccHome.value} -c -x c -o ${of.getAbsolutePath} ${cf.getAbsolutePath}".!
    s"${arPath.value} rcs ${new File(cd, nuklearLibraryOutput.value).getAbsolutePath} ${of.getAbsolutePath}".!
  }
)